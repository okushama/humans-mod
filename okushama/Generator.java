
package okushama;

import java.util.Random;

import okushama.personal.gen.BuildingBase;

import cpw.mods.fml.common.IWorldGenerator;

import net.minecraft.src.IChunkProvider;
import net.minecraft.src.World;

public class Generator implements IWorldGenerator{

	public Schematic instance;
	
	public Generator(Schematic in){
		instance = in;
		
	}
	
	@Override
	public void generate(Random rand, int chunkX, int chunkZ, World w, IChunkProvider gen, IChunkProvider provider){
		BuildingBase.world = w;
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  "+chunkX+" "+chunkZ);
	//	if(!provider.chunkExists(chunkX, chunkZ))
		instance.generateSurface(w, rand, chunkX, chunkZ);
	}
}
