package okushama.humansplus.plugin;

import net.minecraft.src.*;

public interface IPlugin {
	
	public String plugin_name();

	public void game_load();
	
	public void init();
	
	public void world_update(World w);
}
