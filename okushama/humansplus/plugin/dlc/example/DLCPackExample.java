package okushama.humansplus.plugin.dlc.example;

import net.minecraft.src.BiomeGenBase;
import net.minecraft.src.World;
import okushama.humansplus.RegistryTextures;
import okushama.humansplus.plugin.*;

public class DLCPackExample extends PluginBase{
	
	
	/**
	 *  When the plugin is first loaded, init() is called
	 *	for PluginBase inherited methods that need to be
	 *  set up before the game loads
	 */
	@Override
	public void init() {
		// Registers a Human, for the most part!
		this.addHuman(DLCHumanExample.class, "DLC Human Example", 0, BiomeGenBase.beach, BiomeGenBase.plains);
	}
	
	/**
	 *	When the mod is registering everything, use</br>
	 *  this method to register anything extra</br>
	 *  &nbsp;&nbsp;eg.  <b>keybinds</b>, <b>generation</b>, <b>handlers</b>
	 */
	@Override
	public void game_load() {
		// Adds the possible textures for the Human!
		RegistryTextures.registerTexture(DLCHumanExample.class, "dlch1.png;dlch2.png;dlch3.png;dlch4.png");
	}
		
	

	/**
	 *  On tick in the world, when possible, will use the
	 *  Integrated server instance
	 */
	@Override
	public void world_update(World w) {}
	
	
	@Override
	public String plugin_name() {
		return "DLC Pack Example";
	}

}
