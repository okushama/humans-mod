package okushama.humansplus.plugin.dlc.example;

import net.minecraft.src.World;
import okushama.humansplus.Human;
import okushama.humansplus.RegistryTextures;

public class DLCHumanExample extends Human{

	public DLCHumanExample(World par1World) {
		super(par1World);
		name = "DLC Human Example";
		texture = RegistryTextures.getTexture(this, "/okushama/humansplus/plugin/dlc/example/");
	}

}
