package okushama.humansplus.plugin;

import java.util.*;

import okushama.humansplus.Human;
import net.minecraft.src.BiomeGenBase;
import net.minecraft.src.World;

public abstract class PluginBase implements IPlugin{

	public void addHuman(Class<? extends Human> human, String name, int alignment, BiomeGenBase... spawns){
		addedHumans.add(new Manager.PluginHuman(human, name, alignment, spawns));
	}
	
	
	public List<Manager.PluginHuman> addedHumans = new ArrayList<Manager.PluginHuman>();

}
