package okushama.humansplus.plugin;

import java.util.*;

import net.minecraft.client.Minecraft;
import net.minecraft.src.BiomeGenBase;

import okushama.humansplus.Alignment;
import okushama.humansplus.Human;
import okushama.humansplus.RegistryHuman;


public class Manager {
	
	public List<PluginBase> plugins = new ArrayList<PluginBase>();
	
	public Manager(){
		try{
			for(Package p : Package.getPackages()){
				if(!p.getName().startsWith("okushama.humansplus.plugin")) continue;
				for(Class c : PluginFinder.getClassesForPackage(p)){
					if(c.getSuperclass() == PluginBase.class && c != PluginBase.class){
						// Plugin!
						PluginBase plugin = (PluginBase) c.newInstance();
						plugins.add(plugin);
						plugin.init();
						System.out.println("Added plugin "+plugin.plugin_name());
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public void initAllPlugins(){
		for(PluginBase p : plugins){
			p.game_load();
			System.out.println(p.plugin_name()+" loaded successfully!");
		}
	}
	
	public void addAllHumans(RegistryHuman registry){
		for(PluginBase p : plugins){
			for(PluginHuman h : p.addedHumans){
				RegistryHuman.Human asset = new RegistryHuman.Human(h.humanName, h.humanClass, h.humanSpawns);
				registry.humans.add(asset);
				switch(h.humanAlignment){
					case 1:
						Alignment.good.add(h.humanClass);
						break;
					case -1:
						Alignment.evil.add(h.humanClass);
						break;
					default:
						Alignment.neutral.add(h.humanClass);
						break;
				}
			}
		}
	}
	
	public static class PluginHuman{
		public Class<? extends Human> humanClass;
		public String humanName;
		public int humanAlignment;
		public BiomeGenBase[] humanSpawns;
		public PluginHuman(Class<? extends Human> c, String n, int a, BiomeGenBase[] s){
			humanClass = c;
			humanName = n;
			humanAlignment = a;
			humanSpawns = s;
		}
	}

}
