package okushama.humansplus;



import net.minecraft.src.*;



public class ModelSword extends ModelBase
{
  //fields
    ModelRenderer blade;
    ModelRenderer hiltguard;
    ModelRenderer hiltbottom;
    ModelRenderer hilttop;
    ModelRenderer handle, Shape1;
  public ModelSword()
  { 
	  blade = new ModelRenderer(this, 0, 0);
      blade.addBox(0F, 0F, 0F, 30, 2, 1);
      blade.setRotationPoint(-25.55F, 18F, -4F);
      blade.setTextureSize(64, 32);
      blade.mirror = true;
      setRotation(blade, 0F, 0F, 0F);
      hiltguard = new ModelRenderer(this, 0, 3);
      hiltguard.addBox(0F, 0F, 0F, 1, 4, 1);
      hiltguard.setRotationPoint(4.466667F, 17F, -4F);
      hiltguard.setTextureSize(64, 32);
      hiltguard.mirror = true;
      setRotation(hiltguard, 0F, 0F, 0F);
      hilttop = new ModelRenderer(this, 4, 3);
      hilttop.addBox(0F, 0F, 0F, 1, 2, 1);
      hilttop.setRotationPoint(4.8F, 20.33333F, -4F);
      hilttop.setTextureSize(64, 32);
      hilttop.mirror = true;
      setRotation(hilttop, 0F, 0F, 0.8179294F);
      hiltbottom = new ModelRenderer(this, 4, 3);
      hiltbottom.addBox(0F, 0F, 0F, 1, 2, 1);
      hiltbottom.setRotationPoint(5.4F, 17F, -4F);
      hiltbottom.setTextureSize(64, 32);
      hiltbottom.mirror = true;
      setRotation(hiltbottom, 0F, 0F, 2.214193F);
      handle = new ModelRenderer(this, 8, 3);
      handle.addBox(0F, 0F, 0F, 3, 1, 1);
      handle.setRotationPoint(5.4F, 18.5F, -4F);
      handle.setTextureSize(64, 32);
      handle.mirror = true;
      setRotation(handle, 0F, 0F, 0F);
      Shape1 = new ModelRenderer(this, 0, 16);
      Shape1.addBox(-0.1333333F, 0.06666667F, 0F, 4, 12, 4);
      Shape1.setRotationPoint(12.73333F, 28.2F, -4.8F);
      Shape1.setTextureSize(64, 32);
      Shape1.mirror = true;
      Shape1.isHidden = true;
      setRotation(Shape1, -0.1074049F, 0.1611073F, 2.926783F);
	  
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5,entity);
    blade.render(f5);
    hiltguard.render(f5);
    hiltbottom.render(f5);
    hilttop.render(f5);
    handle.render(f5);
    Shape1.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity ent)
  {
    super.setRotationAngles(f, f1, -f2, f3, f4, f5, ent);
  }

}
