package okushama.humansplus;

import java.io.File;
import java.io.FileReader;
import java.util.EnumSet;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

import okushama.humansplus.quest.HandlerQuests;
import okushama.humansplus.quest.ScriptHelper;
import okushama.personal.MoCap;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.common.asm.SideOnly;

import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import cpw.mods.fml.client.registry.KeyBindingRegistry.KeyHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.asm.SideOnly;

@SideOnly(Side.CLIENT)
public class HandlerKeybinds extends KeyHandler{

	public Minecraft mc = Minecraft.getMinecraft();
	public static KeyBinding questKey = new KeyBinding("Humans+ Quest", Keyboard.KEY_H),
							 debugKey = new KeyBinding("Humans+ Debug", Keyboard.KEY_O);
	
	public HandlerKeybinds() {
		super(new KeyBinding[]{questKey, debugKey}, new boolean[]{false, false});
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return "Humans+ Key Bindings";
	}

	public static boolean showPanel = false;
	int test = 0;
	@Override
	public void keyDown(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd, boolean isRepeat) {
	//	System.out.println(kb.keyCode+" "+questKey.keyCode+" "+isRepeat);

		if(kb.keyCode == questKey.keyCode && tickEnd && mc.currentScreen == null){
			if(HandlerQuests.currentQuest == null){
			//	HandlerQuests.setQuest("okushama/humansplus/tquest2.txt");
				//HandlerQuests.currentQuest.initQuest();
			}else{
				showPanel = !showPanel;
				if(!HandlerQuests.currentQuest.hasStarted){
					//HandlerQuests.currentQuest.startQuest();
					//System.out.println("Started Quest!");
				}else{
				//	HandlerQuests.currentQuest.hasFinished = true;
				}
			}
		}
		if(kb.keyCode == debugKey.keyCode && tickEnd && mc.currentScreen == null){
			Vec3 plv = mc.thePlayer.getLookVec();
			MagicList.addEffect("fireball", mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ, plv.xCoord/2, plv.yCoord/2, plv.zCoord/2);
			MagicList.getWorldForPlayer().playSoundAtEntity(mc.thePlayer, "okushama.humansplus.fireballcast", 0.7F, 1F);
			//mc.theWorld.playSoundAtEntity(mc.thePlayer, "okushama.humansplus.bill", 0.7F, 1F);
		}
		
	}

	@Override
	public void keyUp(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd) {
		
	}

	@Override
	public EnumSet<TickType> ticks() {
		// TODO Auto-generated method stub
		return EnumSet.of(TickType.CLIENT);
	}
	

}
