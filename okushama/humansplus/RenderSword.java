package okushama.humansplus;

import net.minecraft.client.Minecraft;
import net.minecraft.src.Entity;
import net.minecraft.src.ModelBase;
import net.minecraft.src.Render;
import net.minecraft.src.RenderLiving;
import net.minecraftforge.client.ForgeHooksClient;

public class RenderSword extends Render{

	public static ModelBase model;
	public String tex;
	public RenderSword(ModelBase modelType, String s) {
		super();
		model = modelType;
		tex = s;
	}
	@Override
	public void doRender(Entity var1, double var2, double var4, double var6, float var8, float var9) {
		ForgeHooksClient.bindTexture(tex, 0);
		model.render(var1, 0F, 0F, 0F, Minecraft.getMinecraft().thePlayer.rotationYawHead-180F, var9, 1F);
		ForgeHooksClient.unbindTexture();
	}


}
