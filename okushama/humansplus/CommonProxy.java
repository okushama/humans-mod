package okushama.humansplus;

import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.client.registry.KeyBindingRegistry;
import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.registry.TickRegistry;

public class CommonProxy {
	
	public void init(){
	
		// Update 
		TickRegistry.registerTickHandler(new HandlerLogic(), Side.CLIENT);
		MinecraftForgeClient.preloadTexture("/okushama/humansplus/swords.png");

		
		// Hook Events
		MinecraftForge.EVENT_BUS.register(new HandlerEvent());
	}

}
