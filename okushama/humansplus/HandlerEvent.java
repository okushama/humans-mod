package okushama.humansplus;

import java.util.List;

import okushama.humansplus.quest.MonitorBounty;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.client.event.sound.PlaySoundEvent;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.item.ItemExpireEvent;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.player.*;

public class HandlerEvent {
	
	
	@ForgeSubscribe
	public void onPlayerPickupItem(EntityItemPickupEvent evt){
		EntityPlayer p = evt.entityPlayer;
		EntityItem it = evt.item;
		
	}
	
	@ForgeSubscribe
	public void onLivingDie(LivingDeathEvent evt){
		
		if(evt.source.getSourceOfDamage() != null){
			if(evt.source.getSourceOfDamage() instanceof EntityPlayer){
				EntityPlayer thePlayer = Minecraft.getMinecraft().thePlayer;
				if(Minecraft.getMinecraft().getIntegratedServer() != null){
					List<EntityPlayer> l = Minecraft.getMinecraft().getIntegratedServer().getConfigurationManager().playerEntityList;
					for(int k = 0; k < l.size(); k++){
						if(l.get(k).username.equals(thePlayer.username)){
							thePlayer = l.get(k);
						}
					}
				}
				if(evt.source.getSourceOfDamage() == thePlayer){
					// Was the player that killed the mob!
					if(MonitorBounty.isMonitoring){
						if(evt.entity instanceof Human){
							MonitorBounty.assessKillToMob(((Human)evt.entity).name);
						}else{
							MonitorBounty.assessKillToMob(evt.entity.getEntityName());
						}
					}
				}
			}
		}
	}
	

	@ForgeSubscribe
	public void itemToss(ItemTossEvent evt){
		/*Minecraft.getMinecraft().ingameGUI.getChatGUI().printChatMessage("Upon attempting to drop the item, you can't let go.");
		evt.player.inventory.setInventorySlotContents(evt.player.inventory.currentItem, evt.entityItem.item);
		evt.entityItem.setDead();*/
	}
	
	@ForgeSubscribe
	public void itemExpire(ItemExpireEvent evt){
		EntityItem item = evt.entityItem;
		if(item.item.getItem() instanceof Sword){
			Sword sword = (Sword)item.item.getItem();
			if(sword.getRarity(item.item) == RegistrySword.legendary)
				Minecraft.getMinecraft().ingameGUI.getChatGUI().printChatMessage("A Legendary Sword just expired!");
		}
	}
	

}
