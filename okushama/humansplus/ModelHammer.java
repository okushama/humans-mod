package okushama.humansplus;

import net.minecraft.src.*;

public class ModelHammer extends ModelBase
{
  //fields
    ModelRenderer blade;
    ModelRenderer hammerhead;
  
  public ModelHammer()
  {
    textureWidth = 64;
    textureHeight = 32;
    
    blade = new ModelRenderer(this, 0, 0);
    blade.addBox(0F, 0F, 0F, 30, 2, 1);
    blade.setRotationPoint(-15.55F, 4F, 0F);
    blade.setTextureSize(64, 32);
    blade.mirror = true;
    setRotation(blade, 0F, 0F, 0F);
    hammerhead = new ModelRenderer(this, 16, 16);
    hammerhead.addBox(0F, 0F, 0F, 5, 10, 6);
    hammerhead.setRotationPoint(-15.2F, -0.5F, -2.4F);
    hammerhead.setTextureSize(64, 32);
    hammerhead.mirror = true;
    setRotation(hammerhead, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    blade.render(f5);
    hammerhead.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
  }

}
