package okushama.humansplus;

import net.minecraft.client.Minecraft;
import net.minecraft.src.DamageSource;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.Tessellator;
import net.minecraft.src.Vec3;
import net.minecraft.src.World;
import net.minecraftforge.client.ForgeHooksClient;

public class MagicFireball extends MagicFX{

	public MagicFireball(World par1World, double par2, double par4,
			double par6, double par8, double par10, double par12) {
		super(par1World, par2, par4, par6, par8, par10, par12);
		this.particleMaxAge = 200;
		this.cachedMotionX = par8;
		this.cachedMotionY = par10;
		this.cachedMotionZ = par12;
		this.particleGravity = 0F;
		this.alpha = 0F;
	}
	
	public double cachedMotionX, cachedMotionY, cachedMotionZ;
	
	public MagicFireball(int tx, int ty){
		super(tx, ty);
	}
	
	private void generateRandomParticles(Entity ent, String par1Str)
    {
        for (int var2 = 0; var2 < 5; ++var2)
        {
            double var3 = this.rand.nextGaussian() * 0.02D;
            double var5 = this.rand.nextGaussian() * 0.02D;
            double var7 = this.rand.nextGaussian() * 0.02D;
            this.worldObj.spawnParticle(par1Str, ent.posX + (double)(this.rand.nextFloat() * ent.width * 2.0F) - (double)ent.width, ent.posY + 1.0D + (double)(this.rand.nextFloat() * ent.height), ent.posZ + (double)(this.rand.nextFloat() * ent.width * 2.0F) - (double)ent.width, var3, var5, var7);
        }
    }
	
	public boolean hasFired = false;
	public boolean gotInitialOffset = false;
	public double offsetX, offsetY, offsetZ;
	
	public void onUpdate(){
		this.particleAge++;
		if(this.particleAge % 5 == 0){
			this.textureX++;
			if(this.textureX > 3){
				this.textureX = 0;
			}
		}
		this.prevPosX = this.posX;
	    this.prevPosY = this.posY;
	    this.prevPosZ = this.posZ;
		if(this.particleAge < 30 && !hasFired){
			this.particleScale += 0.1F;
			if(this.alpha < 0.4F){
				this.alpha+= 0.1F;
			}
			EntityPlayer p = Minecraft.getMinecraft().thePlayer;
			if(!gotInitialOffset){
				offsetX = this.posX - p.posX;
				offsetY = this.posY - p.posY;
				offsetZ = this.posZ - p.posZ;
				gotInitialOffset = true;
			}
			this.setPosition(p.posX+offsetX, p.posY+offsetY, p.posZ+offsetZ);
			return;
		}else{
			if(!hasFired){
				Vec3 l = Minecraft.getMinecraft().thePlayer.getLookVec();
				this.cachedMotionX = l.xCoord;
				this.cachedMotionY = l.yCoord;
				this.cachedMotionZ = l.zCoord;
			
			}
			hasFired = true;
		}
		if(this.alpha < 1F){
			this.alpha += 0.1F;
		}
	    this.moveEntity(this.cachedMotionX, this.cachedMotionY, this.cachedMotionZ);
		for(Object e : worldObj.loadedEntityList){
			if(e instanceof EntityLiving){
				EntityLiving l = (EntityLiving)e;
				if(this.boundingBox.expand(0.5D, 0.5D, 0.5D).intersectsWith(l.boundingBox)){
					if(!(l instanceof EntityPlayer)){
						MagicList.getServerEnt(l).setFire(10);
						this.setDead();
						MagicList.getServerEnt(l).attackEntityFrom(DamageSource.magic, 5);
						this.generateRandomParticles(l, "smoke");
						this.generateRandomParticles(l, "fire");
						MagicList.getWorldForPlayer().playSoundAtEntity(this, "okushama.humansplus.fireballhit", 0.7F, 1F);
					}
				}
			}
		}
		if(this.particleAge > this.particleMaxAge || this.isCollided){
			this.setDead();
		}
	}

	@Override
	public void renderParticle(Tessellator par1Tessellator, float par2,
			float par3, float par4, float par5, float par6, float par7) {
		super.renderParticle(par1Tessellator, par2, par3, par4, par5, par6, par7);
	}
	
	



}
