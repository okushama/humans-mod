package okushama.humansplus;

import java.util.HashMap;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.World;

public class MagicList {
	
	public static HashMap<String, MagicFX> magicTable = new HashMap<String, MagicFX>();
	
	
	static{
		magicTable.put("test", new MagicFX(0,0));	
		magicTable.put("fireball", new MagicFireball(0,0));
	}
	
	public static World getWorldForPlayer(){
		if(Minecraft.getMinecraft().getIntegratedServer() != null){
			return Minecraft.getMinecraft().getIntegratedServer().worldServerForDimension(Minecraft.getMinecraft().thePlayer.dimension);
		}
		return Minecraft.getMinecraft().theWorld;
	}
	
	/** Gets server-side instance of a given entity,
	 *  If an instance can't be found, returns client instance!
	 *  */
	public static EntityLiving getServerEnt(EntityLiving l){
		if(Minecraft.getMinecraft().getIntegratedServer() != null){
			List list = Minecraft.getMinecraft().getIntegratedServer().worldServerForDimension(Minecraft.getMinecraft().thePlayer.dimension).loadedEntityList;
			for(int i =0; i < list.size(); i++){
				Entity ent = (Entity)list.get(i);
				if(ent instanceof EntityLiving)
					if(ent.entityId == l.entityId){
						return (EntityLiving) ent;
					}
			}
		}
		return l;
	}
	
	
	public static void addEffect(String type, double x, double y, double z, double mx, double my, double mz){
		try{
			MagicFX mfx = magicTable.get(type);
			Class c = mfx.getClass();
			Class[] partypes = {World.class, double.class, double.class, double.class, double.class, double.class, double.class};
			Object[] pars     = {Minecraft.getMinecraft().theWorld, x, y, z, mx, my, mz};
			mfx =  (MagicFX) c.getDeclaredConstructor(partypes).newInstance(pars);
			System.out.println("Rendering "+mfx.getClass().getName());
			Minecraft.getMinecraft().effectRenderer.addEffect(mfx, mfx);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
