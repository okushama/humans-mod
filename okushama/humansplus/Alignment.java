package okushama.humansplus;

import java.util.ArrayList;

import net.minecraft.src.*;

public class Alignment implements IEntitySelector{
	
	public static int playerAlignment = 0;
	
	public static ArrayList<Class> 
	good = new ArrayList<Class>(),
	evil = new ArrayList<Class>(),
	undead = new ArrayList<Class>(),
	neutral = new ArrayList<Class>();
	
	
	public static Class[] GOOD = {   HumanHunter.class, HumanSamurai.class, 
								     EntityIronGolem.class};
	
	public static Class[] EVIL = {   HumanRogue.class, HumanBandit.class};
	
	public static Class[] UNDEAD = { EntityZombie.class, EntitySpider.class, 
									 EntitySkeleton.class, EntityEnderman.class,
									 EntitySilverfish.class, EntityWitch.class};
	
	public static Class[] NEUTRAL = {EntityPlayer.class, EntityVillager.class};
	
	public static Class[] getAlignment(Class c){
		for(Class c2 : GOOD){
			if(c2 == c){
				return GOOD;
			}
		}
		for(Class c2 : EVIL){
			if(c2 == c){
				return EVIL;
			}
		}
		for(Class c2 : UNDEAD){
			if(c2 == c){
				return UNDEAD;
			}
		}
		for(Class c2 : NEUTRAL){
			if(c2 == c){
				return NEUTRAL;
			}
		}
		return new Class[]{};
	}
	public static Class[][] getTargets(Class c){
		for(Class c2 : GOOD){
			if(c2 == c){
				return new Class[][]{EVIL, UNDEAD};
			}
		}
		for(Class c2 : EVIL){
			if(c2 == c){
				return new Class[][]{GOOD, UNDEAD};
			}
		}
		for(Class c2 : UNDEAD){
			if(c2 == c){
				return new Class[][]{GOOD, EVIL, NEUTRAL};
			}
		}
		for(Class c2 : NEUTRAL){
			if(c2 == c){
				return new Class[][]{};
			}
		}
		return new Class[][]{};
	}
	
	public static ArrayList[] getTargetsForMob(Class c){
		for(Class r : good){
			if(r == c){
				return new ArrayList[]{evil, undead};
			}
		}
		for(Class r : evil){
			if(r == c){
				return new ArrayList[]{good, undead};
			}
		}
		for(Class r : undead){
			if(r == c){
				return new ArrayList[]{evil, good, neutral};
			}
		}
		return new ArrayList[0];
	}
	
	

	static{
		for(Class c : GOOD){
			good.add(c);
		}
		for(Class c : EVIL){
			evil.add(c);
		}
		for(Class c : NEUTRAL){
			neutral.add(c);
		}
		for(Class c : UNDEAD){
			undead.add(c);
		}
		
	}



	@Override
	public boolean isEntityApplicable(Entity var1) {
		return false;
	}

}
