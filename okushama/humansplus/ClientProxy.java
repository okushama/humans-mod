package okushama.humansplus;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.ForgeHooksClient;
import net.minecraftforge.client.MinecraftForgeClient;
import cpw.mods.fml.client.registry.KeyBindingRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy{
	
	@Override
	public void init(){
		// Keybinds
		KeyBindingRegistry.registerKeyBinding(new HandlerKeybinds());
		for(RegistryHuman.Human h : RegistryHuman.humans){
			 // Setting Render
			RenderingRegistry.registerEntityRenderingHandler(h.entClass, new RenderHuman()); 	
			MinecraftForgeClient.registerItemRenderer(RegistrySword.swords.get(6).theSword.shiftedIndex, new GauntletRender());

		}
	//	RenderingRegistry.registerEntityRenderingHandler(EntitySword.class, new RenderSword());
		super.init();
	}

}
