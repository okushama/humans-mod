package okushama.humansplus;

import java.util.ArrayList;

import okushama.Generator;
import okushama.Schematic;
import okushama.humansplus.plugin.Manager;
import okushama.humansplus.plugin.PluginBase;


import net.minecraft.src.*;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.client.registry.*;
import cpw.mods.fml.common.*;
import cpw.mods.fml.common.Mod.*;
import cpw.mods.fml.common.event.*;
import cpw.mods.fml.common.network.*;
import cpw.mods.fml.common.registry.*;

@Mod(modid = "humansPlus", name = "Humans+", version = "3.0a2")
@NetworkMod(clientSideRequired = false, serverSideRequired = false)

public class ModHumansPlus {
	
	@Instance("humansPlus")
	public static ModHumansPlus instance;
	public static RegistrySword swords; 
	public static RegistryHuman humanRegistry;
	public static Schematic input;
	public static Generator gen;
	public static Manager plugins = new Manager();
	
	@SidedProxy(clientSide="okushama.humansplus.ClientProxy", serverSide="okushama.humansplus.CommonProxy")
	public static CommonProxy proxy;
	
	@PreInit
	public void preInit(FMLPreInitializationEvent event) {
	}

	@Init
	public void init(FMLInitializationEvent event) {
		ModHumansPlus.humanRegistry = new RegistryHuman();
		ModHumansPlus.swords = new RegistrySword();
		input = new Schematic();
		gen = new Generator(input);
		GameRegistry.registerWorldGenerator(gen);
		plugins.initAllPlugins();
		input.init();
		proxy.init();
	}

	@PostInit
	public static void postInit(FMLPostInitializationEvent event) {
		
	}

	

}