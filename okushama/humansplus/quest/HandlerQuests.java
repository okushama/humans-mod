package okushama.humansplus.quest;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import javax.script.ScriptException;

import okushama.humansplus.HandlerKeybinds;
import okushama.humansplus.Human;
import okushama.humansplus.ModHumansPlus;
import okushama.humansplus.Panel;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;

import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

public class HandlerQuests {
	
	public Minecraft mc = Minecraft.getMinecraft();
	public EntityPlayer thePlayer = null;
	public World theWorld = null;
	public ModHumansPlus humansMod = new ModHumansPlus();
	
	public static Quest currentQuest = null;
	public ArrayList<Quest> loadedQuests = new ArrayList<Quest>();
	public ArrayList<String> finishedQuests = new ArrayList<String>();
	
	public static void setQuest(String s){
		currentQuest = new Quest(s);
	}
	
	public static HandlerQuests instance;
	
	public HandlerQuests(){
		instance = this;
		Panel p = new PanelQuest();
		Panel p1 = new PanelQuestImage(266, 17, 40, 40, "/okushama/humansplus/questicons.png",0,0);
		panels.add(p);
		panels.add(p1);
		for(String f : new File(Minecraft.getMinecraftDir().getAbsolutePath()+"/okushama/humansplus/quest/").list()){
			if(f.endsWith("1.txt")){
				System.out.println("Loaded Quest! "+f);
				loadedQuests.add(new Quest("/okushama/humansplus/quest/"+f));
			}
		}
		for(String f : new File(Minecraft.getMinecraftDir().getAbsolutePath()+"/okushama/humansplus/quest/").list()){
			if(f.startsWith("s_") && f.endsWith("1.txt")){
				System.out.println("Loaded Story Quest! "+f);
				loadedQuests.add(new Quest("/okushama/humansplus/quest/"+f));
			}
		}
	}
	
	int finishedTimer = 100;
	boolean startFinishTimer = false;
	public static String nextQuest = null;
	public static Human nextVendor = null;
	public boolean set = false;
	
	public int timeout = 680;
	public void onTick(){
		if(theWorld == null || thePlayer == null){
			if(mc.thePlayer != null){
				thePlayer = mc.thePlayer;
				theWorld = thePlayer.worldObj;
			}
			if(currentQuest != null){
				currentQuest = null;
				return;
			}
		}
		if(theWorld != null && thePlayer != null){
			if(currentQuest != null){
				if(currentQuest.nextQuest != null){
					nextQuest = currentQuest.nextQuest;
					nextVendor = currentQuest.vendor;
				}
				if(!currentQuest.hasStarted){
					timeout--;
					if(timeout == 0){
						currentQuest.quitQuest();
						timeout = 6800;
						System.out.println("Quest timed out!");
						return;
					}
				}else{
					timeout = 6800;
				}
				if(currentQuest.vendor != null){
					if(currentQuest.vendor.isDead){
						currentQuest.quitQuest();
						return;
					}
					if(currentQuest.vendor.getEntityToAttack() instanceof EntityPlayer){
						currentQuest.quitQuest();
						return;
					}
					if(thePlayer != null)
					if(currentQuest.vendor.getDistanceToEntity(thePlayer) < 10D){
						currentQuest.vendor.isInRangeToVend = true;
					}else{
						currentQuest.vendor.isInRangeToVend = false;
					}
					currentQuest.onUpdateDuringQuest();
					if(currentQuest == null){
						return;
					}
				}
				if(currentQuest.hasFinished){
					if(currentQuest.vendor == null){
						return;
					}
					if(!startFinishTimer){
						if(currentQuest.vendor != null)
							currentQuest.vendor.setVendingQuest(false);
						finishedQuests.add(currentQuest.name);
						((PanelQuest)panels.get(0)).setLines();
						startFinishTimer = true;
						return;
					}else{
						finishedTimer--;
						if(finishedTimer == 0){
							currentQuest = null;
							finishedTimer = 100;
							startFinishTimer = false;
							return;
						}
					}
				}
				if(currentQuest.hasStarted){
					((PanelQuest)panels.get(0)).setLines();
				}
			}else{
				if(mc.getIntegratedServer() == null){
					return;
				}
				int random = new Random().nextInt(loadedQuests.size());
				Quest q = loadedQuests.get(random);
				//for(Quest q : loadedQuests)
				{
					boolean finished = false;
					for(String fq : finishedQuests){
						if(q.name.equals(fq)){
							finished = true;
						}
					}
					if(finished && nextQuest == null){
						return;
					}
					int rand = new Random().nextInt(300);
					if(rand == 0)
					{
						if(nextQuest != null){
						//	System.out.println("Quest String!!!");
							this.setQuest(nextQuest);
							currentQuest.vendor = nextVendor;
							currentQuest.vendor.setVendingQuest(true);
							currentQuest.vendor.setName(currentQuest.npcname);
							((PanelQuest)panels.get(0)).setQuest(currentQuest);
							currentQuest.initQuest();
							nextQuest = null;
							nextVendor = null;
						}else{
							this.setQuest("/okushama/humansplus/quest/"+q.questConfig.getName());
							((PanelQuest)panels.get(0)).setQuest(currentQuest);
							currentQuest.initQuest();
							HandlerKeybinds.showPanel = true;
						//	currentQuest.startQuest();
							nextQuest = null;
							nextVendor = null;
							return;
						}
					}
				}
			}
		}
	}
	
	public static void nextQuest(){
		setQuest(nextQuest);
		currentQuest.vendor = nextVendor;
		currentQuest.vendor.setVendingQuest(true);
		currentQuest.vendor.setName(currentQuest.npcname);
		((PanelQuest)panels.get(0)).setQuest(currentQuest);
		currentQuest.initQuest();
		nextQuest = null;
		nextVendor = null;
	}
	
	public static ArrayList<Panel> panels = new ArrayList<Panel>();
	
	public String getQuestType(int i){
		String s = "";
		if(i == Quest.COLLECTION){
			s = "Collection";
		}
		if(i == Quest.SCOUT){
			s = "Scout";
		}
		if(i == Quest.HUNTED){
			s = "Hunted";
		}
		if(i == Quest.BOUNTY){
			s = "Bounty";
		}
		return s;
	}

	//@SideOnly(Side.CLIENT)
	public void onRenderTick(){
		if(currentQuest != null && mc.currentScreen == null){
			if(currentQuest.vendor != null && HandlerKeybinds.showPanel){
				if(currentQuest.vendor.isInRangeToVend){
					for(Panel p : panels){
						p.onUpdate();
						if(!p.visible || mc.currentScreen != null || thePlayer == null ){ continue; }
						GL11.glPushMatrix();
						GL11.glScalef(0.86F,  0.86F, 1F);
						GL11.glPushMatrix();
						mc.renderEngine.bindTexture(mc.renderEngine.getTexture(p.texture));	
						GL11.glEnable(GL11.GL_BLEND);
				        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				        GL11.glTranslatef((float)p.x, (float)p.y, 0F);
						mc.ingameGUI.drawTexturedModalRect(0, 0, 0, 0, p.w, p.h);
						GL11.glDisable(GL11.GL_BLEND);
						GL11.glPopMatrix();
						for(int i = 0; i < p.lines.size(); i++){
							String s = p.lines.get(i);
							mc.fontRenderer.drawString(s, p.linesX, p.linesY+i*13, 0xFFFFFF);
						}
						if(p instanceof PanelQuestImage){
							((PanelQuestImage)p).drawImageInside();
						}
						GL11.glPopMatrix();
						
					}
				}else{
					GL11.glPushMatrix();
					GL11.glScalef(0.86F, 0.86F, 1F);
					String loc = (int)currentQuest.vendor.posX+" "+(int)currentQuest.vendor.posY+" "+(int)currentQuest.vendor.posZ;

					if(!currentQuest.objectives.contains(";")){
						currentQuest.objectives = currentQuest.objectives+";";
					}
					String[] objtv = currentQuest.objectives.split(";");
					drawString("Quest: \u00A7f"+currentQuest.name, 10, 30, 0xCCCA56);
				//	drawString(currentQuest.getGuiText(),10, 43, 0xCCCA56);
					if(currentQuest.hasStarted){
						drawString("Objectives:", 10, 56, 0xCCCA56);
						for(int i = 0; i < objtv.length; i++){
							drawString(objtv[i], 10, 69 + (13*i), 0xCCCA56);
						}
					}
					GL11.glPopMatrix();
				}
			}
		}
		
		
	}
	public void drawString(String s, int x, int y, int c){
		
		mc.fontRenderer.drawString(s, x, y, c);

	}

}
