package okushama.humansplus.quest;

public interface Monitor {
	
	public void resetVars();
	
	public void onUpdate();
	
	public void setVarsToQuest();
	

}
