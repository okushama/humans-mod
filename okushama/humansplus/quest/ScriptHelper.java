package okushama.humansplus.quest;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import okushama.humansplus.*;

import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

public class ScriptHelper {
	
	public Quest theQuest;
	public ScriptHelper(Quest q){
		theQuest = q;
	}
	
	public Entity createEntityInWorld(String s, double x, double y, double z, boolean random){
		World w = Minecraft.getMinecraft().getIntegratedServer().worldServerForDimension(Minecraft.getMinecraft().thePlayer.dimension);
		Entity ent = EntityList.createEntityByName(s, w);
		if(!random){
			ent.setLocationAndAngles(x, y, z, 180, 180);
		}else{
			Random rand = new Random();
			ent.setLocationAndAngles(x+(rand.nextBoolean() ? 10+rand.nextInt(30) : -10-rand.nextInt(30)), y+3, z+(rand.nextBoolean() ? 10+rand.nextInt(30) : -10-rand.nextInt(30)), 180, 180);
		}
		w.spawnEntityInWorld(ent);
		return ent;
	}
	
	public Human createHumanInWorld(String s, double x, double y, double z, boolean random){
		Human human = null;
		World w = Minecraft.getMinecraft().getIntegratedServer().worldServerForDimension(Minecraft.getMinecraft().thePlayer.dimension);
		if(s.toLowerCase().equals("hunter")){		
			human = (HumanHunter) new HumanHunter(w);
		}
		if(s.toLowerCase().equals("rogue")){
			human = (HumanRogue) new HumanRogue(w);
		}
		if(s.toLowerCase().equals("bandit")){
			human = (HumanBandit) new HumanBandit(w);
		}
		if(s.toLowerCase().equals("samurai")){
			human = (HumanSamurai) new HumanSamurai(w);
		}
		if(human == null){
			return null;
		}
		if(!random){
			human.setLocationAndAngles(x, y, z, 180, 180);
		}else{
			Random rand = new Random();
			human.setLocationAndAngles(x+(rand.nextBoolean() ? 10+rand.nextInt(30) : -10-rand.nextInt(30)), y+3, z+(rand.nextBoolean() ? 10+rand.nextInt(30) : -10-rand.nextInt(30)), 180, 180);
		}
		w.spawnEntityInWorld(human);		
		return human;
	}
	
	public void setHeldItem(Human h, int itemID){
		if(h != null)
			if(Item.itemsList[itemID] != null)
				h.heldItem = new ItemStack(Item.itemsList[itemID]);
	}
	
	private PathEntity pathFromTo(Entity e, Entity e2){
		return e.worldObj.getPathEntityToEntity(e, e2, 35F, true, false, false, true);
	}
	
	public void setPath(Human mover, EntityLiving movee){
		if(mover.isAIEnabled()){
			mover.getNavigator().tryMoveToEntityLiving(movee, 0.3F);
		}else{
		mover.setPathToEntity(pathFromTo(mover,movee));
		}
	}
	
	public int getDistance(Entity ent, Entity ent2){
		return (int)ent.getDistanceToEntity(ent2);
	}
	
	public void setMoveSpeed(Human h, float s){
		h.setMoveSpeed(s);
	}
	
	public void setName(Human h, String n){
		h.setName(n);
	}
	
	public void playSound(String s, Entity src, float v, float p){
		src.worldObj.playSoundAtEntity(src, s, v, p);
	}
	
	public void setDropItem(Human h, int itemID){
		if(h != null)
			if(Item.itemsList[itemID] != null)
				h.dropItem = new ItemStack(Item.itemsList[itemID]);
	}
	
	public void nextQuest(){
		
	}
	

}
