package okushama.humansplus.quest;

import java.util.*;

import net.minecraft.src.*;

public class MonitorBounty implements Monitor{

	public String flags;
	static ArrayList<String> targetTypes = new ArrayList<String>();
	static HashMap<String, Integer> amountToKill = new HashMap<String, Integer>();		
	public static boolean isMonitoring = false;
	
	public MonitorBounty(){
		
	}
	

	
	@Override
	public void resetVars() {
		flags = "";
		targetTypes = new ArrayList<String>();
		amountToKill = new HashMap<String, Integer>();
		isMonitoring = false;
	}
	
	
	public static void assessKillToMob(String s){
		for(String s1 :targetTypes){
			if(s.toLowerCase().equals(s1.toLowerCase())){
				// We're hunting that mob!
				int mount = amountToKill.get(s1)-1;
				amountToKill.put(s1, mount);		
				System.out.println(s+" "+mount);
				continue;
			}
		}
	}

	@Override
	public void onUpdate() {
		int temp = targetTypes.size();
		if(temp > 0){
			for(String s : targetTypes){
				if(amountToKill.get(s) != null){
					if(amountToKill.get(s) <=0){
						temp--;
					}
				}
			}
			if(temp == 0){
				HandlerQuests.currentQuest.meetRequirements();
				resetVars();
			}
		}
	}

	@Override
	public void setVarsToQuest() {
		flags = HandlerQuests.currentQuest.flags;
		System.out.println(flags);
		String[] flag = flags.split(";");
		for(int i = 0; i < flag.length; i++){
			if(flag[i].contains("*")){
				try{
					String[] target = flag[i].split("\\*");
					System.out.println("Adding Target Type: "+target[0]+" for kill amount "+target[1]);

					targetTypes.add(target[0]);
					amountToKill.put(targetTypes.get(targetTypes.size()-1), Integer.parseInt(target[1]));
				}catch(Exception e){
					e.printStackTrace();
				}
			}else{
				System.out.println("Didn't contain * ?");
			}
		}
		for(String s : targetTypes){
			System.out.println(s+" "+amountToKill.get(s));
		}
		isMonitoring = true;
	}

}
