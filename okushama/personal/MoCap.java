package okushama.personal;

import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

import org.lwjgl.opengl.GL11;

import okushama.humansplus.quest.HandlerQuests;

import net.minecraft.client.Minecraft;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityOtherPlayerMP;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EntityPlayerMP;
import net.minecraft.src.Packet11PlayerPosition;
import net.minecraft.src.Packet12PlayerLook;
import net.minecraft.src.Packet13PlayerLookMove;
import net.minecraft.src.Packet18Animation;
import net.minecraft.src.Packet33RelEntityMoveLook;

public class MoCap {
	
	public MoCap(){

	}
	
	public static boolean isRecording = false, shouldSave = false;
	public static Minecraft mc = Minecraft.getMinecraft();
	public static ArrayList<MoCapState> recording = new ArrayList<MoCapState>();
	public static long recordingLength = 0L;
	public static ArrayList<MoCapRecording> recordings = new ArrayList<MoCapRecording>();
	public static int currentRecording = 0;
	

	
	public static MoCapRecording getSelectedRecording(){
		if(currentRecording >= recordings.size()){
			return null;
		}
		return recordings.get(currentRecording);
	}
	
	public static void startRecording(){
		if(!isRecording){
			recording = new ArrayList<MoCapState>();
			isRecording = true;
			recordingLength = 0L;
		}
	}
	
	public static void stopRecording(boolean save){
		if(isRecording){
			isRecording = false;
			recordings.add(new MoCapRecording(recording, recordingLength));
			if(save){
				Calendar c = Calendar.getInstance();
				SimpleDateFormat f = new SimpleDateFormat("[kk.mm]-dd-MM-yyyy");
				saveRecording(f.format(c.getTime())+".oku");		
			}
		}
	}
	

	public static void startPlayback(){
		try{
			MoCapRecording rec = recordings.get(currentRecording);
			rec.startPlayback();
		}catch(Exception e){
			
		}
	}
	
	public static void saveRecording(String filename){
		File dir = new File(Minecraft.getMinecraftDir().getAbsolutePath()+"/okushama/personal/macro/");
		File newRecording = new File(dir, filename);
		if(!dir.exists()){
			dir.mkdirs();
		}
		if(!newRecording.exists()){
			try {
				if(newRecording.createNewFile()){
					mc.ingameGUI.getChatGUI().printChatMessage("Macro Recording Saved! "+newRecording.getName());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		newRecording = new File(dir, filename);
		try {
			PrintWriter out = new PrintWriter(new FileOutputStream(newRecording));
			for(MoCapState r : recording){
				out.println(r.posX+"|"+r.posY+"|"+r.posZ+"|"+r.motionX+"|"+r.motionY+"|"+r.motionZ+"|"+r.pitch+"|"+r.yaw+"|"+r.headyaw+"|"+r.isSneaking+"|"+r.isSprinting+"|"+r.isJumping+"|"+r.isSwinging+"|"+r.swingProgress+"|"+r.swingProgressInt);
			}
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	

	
	public void onTick(){
		if(mc.thePlayer != null){
			if(isRecording){
				recording.add(new MoCapState(mc.thePlayer));
				recordingLength++;
			}
			for(MoCapRecording rec : recordings){
				if(rec.isPlaying){
					if(rec.isForward){
						if(rec.playbackTime+1 < rec.recordingLength){
							rec.playbackTime++;
								if(rec.recording.get((int)rec.playbackTime) != null){
									MoCapState r = rec.recording.get((int) rec.playbackTime);
									if(rec.targetEnt != null){
										setToMacro(rec.localTargetEnt, r);
										setToMacro(rec.targetEnt, r);
									}else{
										setToMacro(mc.thePlayer, r);
									}									
							}
						}else{
							if(rec.shouldLoop){
								rec.stopPlayback();
								rec.startPlayback();
							}else{
								rec.stopPlayback();
							}
						}
					}else{
						if(rec.playbackTime > 0L && rec.playbackTime < rec.recording.size()){
							rec.playbackTime--;
							if(rec.recording.get((int)rec.playbackTime) != null){
								MoCapState r = rec.recording.get((int) rec.playbackTime);
								if(rec.targetEnt != null){
									setToMacro(rec.localTargetEnt, r);
									setToMacro(rec.targetEnt, r);
								}else{
									setToMacro(mc.thePlayer, r);
								}
								
							}
						}else{
							if(rec.shouldLoop){
								rec.stopPlayback();
								rec.startPlayback();
							}else{
								rec.stopPlayback();
							}
						}
	
					}
				}
			}
		}
	}
	
	public void setToMacro(EntityLiving ent, MoCapState r){
		ent.rotationPitch = r.pitch;
		ent.rotationYawHead = r.headyaw;
		ent.rotationYaw = r.yaw;
		ent.setPosition(r.posX, r.posY+ent.yOffset, r.posZ);
		ent.motionX = r.motionX*1.8;
		ent.fallDistance = 0F;
		ent.motionZ = r.motionZ*1.8;
		ent.isJumping = r.isJumping;
		ent.setSneaking(r.isSneaking);
		ent.setSprinting(r.isSprinting);
		ent.isSwingInProgress = r.isSwinging;
		if(r.isSwinging){
			ent.swingProgress = r.swingProgress;
			ent.swingProgressInt = r.swingProgressInt;
		}
		if(ent instanceof EntityPlayerMP){
			EntityPlayerMP mp = (EntityPlayerMP)ent;
			mp.playerNetServerHandler.sendPacketToPlayer(new Packet18Animation(mp, 1));
			mp.playerNetServerHandler.setPlayerLocation(r.posX, r.posY, r.posZ, r.headyaw, r.pitch);
			//mp.playerNetServerHandler.sendPacketToPlayer(new Packet12PlayerLook(Math.min(r.headyaw, -r.headyaw), r.pitch, !r.isJumping));
			//mp.playerNetServerHandler.sendPacketToPlayer(new Packet11PlayerPosition(r.posX, r.posY+r.recorder.yOffset, 1D, r.posZ, true));
		}
	}
	
	public void drawControl(int xOffset, int yOffset, float x, float y, float a){
		GL11.glPushMatrix();
		mc.renderEngine.bindTexture(mc.renderEngine.getTexture("/okushama/personal/mocap/controls.png"));	
		GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glTranslatef(x, y, 0F);
        GL11.glColor4f(1F, 1F, 1F, a);
		mc.ingameGUI.drawTexturedModalRect(0, 0, 16*xOffset, 16*yOffset, 16, 16);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
		Random rand = new Random();
	}
	
	public void drawStringAlpha(String s, int x, int y, int c, float a){
		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glColor4f(1F, 1F, 1F, a);
		mc.ingameGUI.drawString(mc.fontRenderer, s, x, y, c);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
	}
	
	public double test = 0L;
	public void renderUpdate(){
		test++;
		if(isRecording){
			//drawStringAlpha("REC", (mc.displayWidth/2)-30-mc.fontRenderer.getStringWidth("REC"), 10, 0xDD0000, (float)Math.sin(test / 10D)+0.2F);
			drawControl(4,3, mc.displayWidth/2-40F, 10F, (float)Math.sin(test / 30D)+0.2F);
			drawControl(4,2, mc.displayWidth/2-26F, 10F, (float)Math.sin(test / 30D)+0.2F);
		}
		MoCapRecording r = getSelectedRecording();
		if(r != null){
			if(r.isPlaying){
				int dir = r.isForward ? 0 : 3;
				drawControl(dir, 2, mc.displayWidth/2-26F, 30F, 1F);
			}
		}
		
	}
	
	public static class MoCapRecording implements Serializable{
		
		public ArrayList<MoCapState> recording = new ArrayList<MoCapState>();
		public boolean isRecording = false, isPlaying = false, isForward = true, shouldSave = false, shouldLoop = false;
		public EntityLiving targetEnt = null, localTargetEnt = null;
		public long recordingLength = 0L, playbackTime    = 0L;
		
		public MoCapRecording(ArrayList<MoCapState> rec, long len){
			recording = rec;
			recordingLength = len;
		}
		
		public void startPlayback(){
			if(recordingLength == 0L){
				return;
			}
			if(!isRecording && !isPlaying){
				isPlaying = true;
				if(isForward){
					playbackTime = 0L;
				}else{
					playbackTime = recordingLength;
				}
			}
		}
		public void stopPlayback(){
			if(!isRecording && isPlaying){
				isPlaying = false;
				playbackTime = 0L;
			}
		}
		
		
	}

	
	public static class MoCapState{
		public double posX,posY,posZ;
		public float pitch, headyaw ,yaw, swingProgress;
		public double motionX, motionY, motionZ;
		public boolean isSprinting, isJumping, isSneaking, isSwinging;
		public int swingProgressInt;
		public EntityLiving recorder = null;
		
		public MoCapState(EntityLiving ent){
			posX = ent.posX;
			posY = ent.posY-ent.yOffset;
			posZ = ent.posZ;
			motionX = ent.motionX;
			motionY = ent.motionY;
			motionZ = ent.motionZ;
			pitch = ent.rotationPitch;
			yaw = ent.rotationYaw;
			headyaw = ent.rotationYawHead;
			isSprinting = ent.isSprinting();
			isSneaking = ent.isSneaking();
			isJumping = ent.isJumping;
			isSwinging = ent.isSwingInProgress;
			swingProgressInt = ent.swingProgressInt;
			swingProgress = ent.swingProgress;
			
		}
		
	}
}
