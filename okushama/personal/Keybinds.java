package okushama.personal;

import java.io.File;
import java.io.FileReader;
import java.util.EnumSet;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

import okushama.humansplus.quest.HandlerQuests;
import okushama.humansplus.quest.ScriptHelper;
import okushama.personal.MoCap;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.common.asm.SideOnly;

import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import cpw.mods.fml.client.registry.KeyBindingRegistry.KeyHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.asm.SideOnly;

@SideOnly(Side.CLIENT)
public class Keybinds extends KeyHandler{

	public Minecraft mc = Minecraft.getMinecraft();
	public static KeyBinding keyRecord = new KeyBinding("MoCap Record", Keyboard.KEY_NUMPAD8),
							 keyPlay = new KeyBinding("MoCap Play", Keyboard.KEY_NUMPAD5),
							 keyLatch = new KeyBinding("MoCap Target Ent", Keyboard.KEY_NUMPAD0),
							 keyDirection = new KeyBinding("MoCap Play Dir", Keyboard.KEY_DECIMAL),
							 keySave = new KeyBinding("MoCap Should Save", Keyboard.KEY_NUMPAD9),
							 keyLoop = new KeyBinding("MoCap Should Loop", Keyboard.KEY_NUMPAD6),
							 keyUp = new KeyBinding("MoCap UP", Keyboard.KEY_NUMPAD7),
							 keyDown = new KeyBinding("MoCap DOWN", Keyboard.KEY_NUMPAD7);
	
	public Keybinds() {
		super(new KeyBinding[]{keyRecord, keyPlay, keyLatch, keyDirection, keySave, keyLoop, keyUp, keyDown}, new boolean[]{false, false, false, false, false, false, false, false});
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return "MoCap Key Bindings";
	}

	@Override
	public void keyDown(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd, boolean isRepeat) {
		if(kb.keyCode == keyRecord.keyCode && tickEnd && mc.currentScreen == null){
			if(MoCap.isRecording){
				MoCap.stopRecording(MoCap.shouldSave);
			}else{
				MoCap.startRecording();
			}
		}
		if(kb.keyCode == keyPlay.keyCode && tickEnd && mc.currentScreen == null){
			MoCap.MoCapRecording r = MoCap.getSelectedRecording();
			if(r != null)
			{
				if(!r.isPlaying){
					r.startPlayback();
				}else{
					r.stopPlayback();
				}
			}

		}	
		
		if(kb.keyCode == keyUp.keyCode && tickEnd && mc.currentScreen == null){
			if(MoCap.currentRecording+1 <= MoCap.recordings.size())
				MoCap.currentRecording++;
		}
		
		if(kb.keyCode == keyDown.keyCode && tickEnd && mc.currentScreen == null){
			if(MoCap.currentRecording > 0){
				MoCap.currentRecording--;
			}
		}
		if(kb.keyCode == keyLoop.keyCode && tickEnd && mc.currentScreen == null){
			MoCap.MoCapRecording r = MoCap.getSelectedRecording();
			if(r != null)
			{
				r.shouldLoop = !r.shouldLoop;
				mc.ingameGUI.getChatGUI().printChatMessage("MoCap "+MoCap.currentRecording+" Looping "+((r.shouldLoop) ? "Enabled!" : "Disabled!"));
			}
		}	
		
		if(kb.keyCode == keyDirection.keyCode && tickEnd && mc.currentScreen == null){
			MoCap.MoCapRecording r = MoCap.getSelectedRecording();
			if(r != null){
			r.isForward = !r.isForward;
			String s = r.isForward ? "Forwards" : "Backwards";
			//mc.ingameGUI.getChatGUI().printChatMessage("Macro Direction changed to "+s);
			}
		}
		if(kb.keyCode == keySave.keyCode && tickEnd && mc.currentScreen == null){
			MoCap.shouldSave = !MoCap.shouldSave;
			mc.ingameGUI.getChatGUI().printChatMessage("MoCap Saving "+(MoCap.shouldSave ? "Enabled!" : "Disabled!"));
		}
		if(kb.keyCode == keyLatch.keyCode && tickEnd && mc.currentScreen == null){	
			MoCap.MoCapRecording r = MoCap.getSelectedRecording();
			if(r != null){
				if(mc.objectMouseOver != null && mc.objectMouseOver.entityHit != null && mc.objectMouseOver.entityHit instanceof EntityLiving){
					r.localTargetEnt = (EntityLiving) mc.objectMouseOver.entityHit;
					boolean localOnly = true;
					if(Minecraft.getMinecraft().getIntegratedServer() != null){
						List list = Minecraft.getMinecraft().getIntegratedServer().worldServerForDimension(mc.thePlayer.dimension).loadedEntityList;
						for(int i =0; i < list.size(); i++){
							Entity ent = (Entity)list.get(i);
							if(ent instanceof EntityLiving)
								if(ent.entityId == r.localTargetEnt.entityId){
									r.targetEnt = (EntityLiving)ent;
									localOnly = false;
									continue;
								}
						}
					}
					String ins = localOnly ? "C" : "S";
					mc.ingameGUI.getChatGUI().printChatMessage("MoCap "+MoCap.currentRecording+" Latched "+ins+" Entity!");
				}else{
					r.localTargetEnt = null;
					r.targetEnt = null;
					mc.ingameGUI.getChatGUI().printChatMessage("MoCap Latched to Player!");
	
				}
			}
		}
	}

	@Override
	public void keyUp(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd) {
		
	}

	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.CLIENT);
	}
	

}
