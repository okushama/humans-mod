package okushama.personal;

import java.util.EnumSet;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

public class Ticks implements ITickHandler{
	
	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData) {
		if(type.equals(EnumSet.of(TickType.CLIENT))){
			
		}
		
	}

	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData) {
		if(type.equals(EnumSet.of(TickType.CLIENT))){
			try{
				Bot.instance.macro.onTick();
				}catch(Exception e){
					
				}
		}
		if(type.equals(EnumSet.of(TickType.RENDER))){
		//	AutoMovement.instance.onRenderTick();
			Bot.instance.macro.renderUpdate();
		}
	}

	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.CLIENT, TickType.RENDER);
	}

	@Override
	public String getLabel() {
		return "Macro Update";
	}

}
