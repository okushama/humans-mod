package okushama.personal;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;


import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

public class AutoPath implements ITickHandler{

	public static Minecraft mc = Minecraft.getMinecraft();	
	public static World worldRef;
	public static EntityPlayer theplayer;
	public static int targX = 0;
	public static int targY = 0;
	public static int targZ = 0;
	public static PathEntity path;
	public static boolean pathFollowing = false;
	public static boolean camLock = false;
	public static long lastWorldTime;
	public static boolean coordToggle = false;
	
	

	public void onTick(){
		try{
			if (theplayer == null || worldRef == null) {
				forceInit();
			}
			if(theplayer != null && worldRef != null) {
				if (pathFollowing && path != null && lastWorldTime != worldRef.getWorldInfo().getWorldTime()) {
					pathFollow();
					lastWorldTime = worldRef.getWorldInfo().getWorldTime();
				}

				if (theplayer.motionY > -1.1F && !theplayer.isInWater() && !theplayer.isInsideOfMaterial(Material.lava) && !theplayer.isInsideOfMaterial(Material.cactus)) {
					theplayer.fallDistance = 0.0F;

					if (theplayer instanceof EntityClientPlayerMP) {
						try{
						((EntityClientPlayerMP)theplayer).sendQueue.addToSendQueue(new Packet11PlayerPosition(theplayer.motionX, -999D, -999D, theplayer.motionZ, true));
						}catch(Exception e){
							if(!hasWarnedLocal){
								System.err.println("Player Path: Not connected to an external server!");
								hasWarnedLocal = true;
							}
						}
					}
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}	
	}
	
	public boolean hasWarnedLocal = false;

	
	
	public void onRenderTick(){

	}
	
	public static void forceInit() {
		try{
		mc = Minecraft.getMinecraft();
		worldRef = mc.theWorld;
		theplayer = mc.thePlayer;
		}catch(Exception e){
			System.out.println("An unknown error occured!");
		}
	}
	
	public static void pathTo(Entity ent){
		path = ent(ent);
		pathFollowing = true;
	}
	
	public static PathEntity ent(Entity e2){
		return worldRef.getPathEntityToEntity(theplayer, e2, 100F, true, false, false, true);
	}

	public static void pathTo(int x, int y, int z){
		path = worldRef.getEntityPathToXYZ(theplayer, x, y, z, 100F,false,false,false,false);
		pathFollowing = true;
	}

	public static void stopPathing(){
		if(theplayer != null)
		theplayer.setSprinting(false);
		pathFollowing = false;
	}
	public static boolean notPathing(){
		return !pathFollowing;
	}

	public void say(String s){
	}

	public void pathToPlayer(String playerName){
		List list = worldRef.playerEntities;
		for(int i = 0; i < list.size(); i++){
			if(list.get(i) instanceof EntityOtherPlayerMP){
				EntityOtherPlayerMP otherPlayer = (EntityOtherPlayerMP) list.get(i);
				if(otherPlayer.username.toLowerCase().startsWith(playerName.toLowerCase())){
					pathTo((int)otherPlayer.posX,(int)otherPlayer.posY, (int)otherPlayer.posZ);
					System.out.println("Debug info: "+otherPlayer.serverPosX+" "+otherPlayer.serverPosY+" "+otherPlayer.serverPosZ);
					say("Now pathfinding to "+otherPlayer.username+"!");
					return;
				}
			}
		}
		say("Could not find the specified player!");
	}
	
	public void pathFollow() {
		//theplayer.landMovementFactor = 1.0F;
		Vec3 var5 = path.getVectorFromIndex(theplayer, path.getCurrentPathIndex());
		double var6 = (double)(theplayer.width * 1.2F);

		while(var5 != null && var5.squareDistanceTo(theplayer.posX, var5.yCoord, theplayer.posZ) < var6 * var6) {
			path.incrementPathIndex();

			if(path.isFinished()) {
				var5 = null;
				path = null;
			} else {
				var5 = path.getVectorFromIndex(theplayer, path.getCurrentPathIndex());
			}
		}

		int var21 = MathHelper.floor_double(theplayer.boundingBox.minY + 0.5D);
		float angle = 0F;

		if(var5 != null) {
			double var8 = var5.xCoord - theplayer.posX;
			double var10 = var5.zCoord - theplayer.posZ;
			double var12 = var5.yCoord - (double)var21;
			float var14 = (float)(Math.atan2(var10, var8) * 180.0D / 3D) - 90.0F;
			float var15 = var14 - (theplayer.rotationYaw);
			for(angle = theplayer.capabilities.getWalkSpeed(); var15 < -180.0F; var15 += 360.0F) {
				;
			}

			while(var15 >= 180.0F) {
				var15 -= 360.0F;
			}

			if(var15 > 30.0F) {
				var15 = 30.0F;
			}

			if(var15 < -30.0F) {
				var15 = -30.0F;
			}

			if(!camLock)
			theplayer.rotationYaw += (var15);/**/

			if(var12 > 0.0D || theplayer.handleWaterMovement() || theplayer.handleLavaMovement()) {
				theplayer.isJumping = true;
			}
			if (theplayer.isJumping) {
				if (theplayer.onGround) {
					jump(theplayer);
				}
			}
		}
		theplayer.moveEntityWithHeading(0F,3F);
	}
	
	public void jump(EntityLiving e){
		e.motionY = 0.41999998688697815D;
		e.isAirBorne = true;
		e.isJumping = true;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData) {
		
	}

	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData) {
		if(type.equals(EnumSet.of(TickType.CLIENT))){
			onTick();
		}
		if(type.equals(EnumSet.of(TickType.RENDER))){
			onRenderTick();
		}
	}

	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.CLIENT, TickType.RENDER);
	}

	@Override
	public String getLabel() {
		return "Player Path Update";
	}
}
