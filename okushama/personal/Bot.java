package okushama.personal;

import okushama.humansplus.HandlerKeybinds;
import cpw.mods.fml.client.registry.KeyBindingRegistry;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.TickRegistry;
import net.minecraft.src.*;
@Mod(modid = "okubot", name = "okubot", version = "-1A")
@NetworkMod(clientSideRequired = false, serverSideRequired = false)

public class Bot {
	

	@Instance("okubot")
	public static Bot instance;
	public static MoCap macro;
	
	@PreInit
	public void preInit(FMLPreInitializationEvent event) {
	}

	@Init
	public void init(FMLInitializationEvent event) {
		macro = new MoCap();
		TickRegistry.registerTickHandler(new Ticks(), Side.CLIENT);
		KeyBindingRegistry.registerKeyBinding(new Keybinds());
	}
	
	@PostInit
	public static void postInit(FMLPostInitializationEvent event) {
		
	}
	
	

}
