package okushama.personal.gen;

import net.minecraft.src.Material;

public class GenAfro extends BuildingBase{

	@Override
	public void generate(int x, int y, int z){
		world.setBlockAndMetadataWithNotify(x+2,y+0,z+1,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+0,z+4,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+0,z+6,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+0,z+11,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+0,z+14,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+0,z+17,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+0,z+18,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+1,z+1,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+1,z+4,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+1,z+6,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+1,z+11,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+1,z+14,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+1,z+16,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+1,z+19,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+2,z+1,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+2,z+2,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+2,z+3,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+2,z+4,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+2,z+6,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+2,z+11,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+2,z+14,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+2,z+16,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+2,z+19,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+1,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+4,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+6,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+7,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+8,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+9,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+11,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+12,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+13,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+16,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+19,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+0,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+1,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+4,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+5,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+6,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+11,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+14,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+16,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+19,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+0,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+1,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+2,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+3,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+4,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+5,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+6,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+7,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+8,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+9,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+11,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+12,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+13,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+17,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+5,z+18,35,10);
		world.setBlockAndMetadataWithNotify(x+2,y+6,z+0,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+6,z+1,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+6,z+2,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+6,z+3,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+6,z+4,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+6,z+5,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+6,z+6,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+7,z+0,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+7,z+1,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+7,z+2,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+7,z+3,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+7,z+4,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+7,z+5,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+8,z+0,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+8,z+1,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+8,z+2,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+8,z+3,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+8,z+4,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+9,z+1,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+9,z+2,35,12);
		world.setBlockAndMetadataWithNotify(x+2,y+9,z+3,35,12);
	}

	@Override
	public boolean canGenerate(){
		int stored = y;
		for(int i = 0; i < 1; i++){
			if((world.getTopSolidOrLiquidBlock(x+i, z) != stored)){;
			return false;
			}else{
				if(world.getBlockMaterial(x+i, world.getTopSolidOrLiquidBlock(x+i,z), z)== Material.water){
					return false;
				}
				continue;
			}
		}
		for(int i = 0; i < 1; i++){
			if((world.getTopSolidOrLiquidBlock(x, z+i) != stored)){

				return false;
			}else{
				if(world.getBlockMaterial(x+i, world.getTopSolidOrLiquidBlock(x+i,z), z)== Material.water){
					return false;
				}
				continue;
			}

		}
		return true;

	}
}
