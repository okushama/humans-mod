package okushama.personal.gen;

public class BuildingSignTest extends BuildingBase{

	
	public void generate(int x, int y, int z){
		//Standing Sign
		world.setBlockAndMetadataWithNotify(x+3,y+0,z+4,63,8);
		this.setSignText(x+3, y, z+4, "This is a test; Did the sign do eet?;I guess i shall see;I LIKE PIZZA MAN");

		//Wood
		world.setBlockAndMetadataWithNotify(x+6,y+0,z+3,17,1);

		//Wall Signs
		world.setBlockAndMetadataWithNotify(x+5,y+0,z+3,68,4);
		world.setBlockAndMetadataWithNotify(x+6,y+0,z+4,68,3);


		}

	@Override
	public boolean canGenerate() {
		return true;
	}
}

