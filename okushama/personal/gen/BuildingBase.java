package okushama.personal.gen;

import java.util.ArrayList;

import okushama.humansplus.Human;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

public abstract class BuildingBase {

	public Minecraft mc = ModLoader.getMinecraftInstance();
	public static World world;
	public static int x,y,z;
	public abstract void generate(int x, int y, int z);
	public abstract boolean canGenerate();
	
	public BuildingBase(){
		if(mc.theWorld != null)
			world = mc.theWorld;
		if(mc.thePlayer != null){
			x = (int)mc.thePlayer.posX;
			y = (int)mc.thePlayer.posY-1;
			z = (int)mc.thePlayer.posZ;
		}
	}
	
	public void doGenerate(int px, int py, int pz){
		x = px;
		y = py;
		z = pz;
		generate(x, y, z);
	}

	public void spawnEntity(int x, int y, int z, Entity ent){
		if(ent!=null){
			ent.setPositionAndRotation((double)x, (double)y, (double)z, 0, 0);
		//	world.entityJoinedWorld(ent); //Renamed in update
			world.spawnEntityInWorld(ent);
			try{
				if(ent instanceof Human){
					System.out.println("Humans+ is present!");
					Human human = (Human)ent;
				//	human.canDespawn = false;
					return;
				}
			}catch(Exception e){System.out.println("Humans+ isn't present!");}
		}
	}

	public TileEntityChest getChest(int x,int y, int z){
		TileEntityChest chest = null;
		try{
			chest = (TileEntityChest) world.getBlockTileEntity(x, y, z);
		}catch(Exception e){}
		if(chest == null){
			System.out.println("Chest was mismapped!");
			return null;
		}
		return chest;
	}
	
	public void setSignText(int x, int y, int z, String lines){
		String[] theLines = lines.split(";");
		TileEntitySign sign = null;
		try{
			sign = (TileEntitySign) world.getBlockTileEntity(x, y, z);
		}catch(Exception e){}
			if(sign == null){
				System.out.println("Sign was mismapped!");
				return;
			}
			for(int i = 0; i < 4; i++){
				
			}
			sign.signText = theLines;
			return;
	}
	

}
