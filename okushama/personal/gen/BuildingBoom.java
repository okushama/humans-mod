package okushama.personal.gen;

import java.util.Random;

//import okushama.humansplus.HumanSettler;
import net.minecraft.src.Block;
import net.minecraft.src.EntityCreeper;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Material;
//import net.minecraft.src.mod_PlayerPath;

public class BuildingBoom extends BuildingBase{

	public boolean canGenerate(){
		int stored = y;
		for(int i = 0; i < 5; i++){
			if((world.getTopSolidOrLiquidBlock(x+i, z) != stored)){;
			return false;
			}else{
				if(world.getBlockMaterial(x+i, world.getTopSolidOrLiquidBlock(x+i,z), z)== Material.water){
					return false;
				}
				continue;
			}
		}
		for(int i = 0; i < 6; i++){
			if((world.getTopSolidOrLiquidBlock(x, z+i) != stored)){

				return false;
			}else{
				if(world.getBlockMaterial(x+i, world.getTopSolidOrLiquidBlock(x+i,z), z)== Material.water){
					return false;
				}
				continue;
			}

		}
		return true;

	}

	public void generate(int x, int y, int z){
		y=y-6;
		world.setBlockWithNotify(x+5,y+0,z+4,1);
		world.setBlockWithNotify(x+5,y+0,z+5,1);
		world.setBlockWithNotify(x+5,y+0,z+6,1);
		world.setBlockWithNotify(x+5,y+0,z+7,1);
		world.setBlockWithNotify(x+5,y+1,z+4,1);
		world.setBlockWithNotify(x+5,y+1,z+5,1);
		world.setBlockWithNotify(x+5,y+1,z+6,1);
		world.setBlockWithNotify(x+5,y+1,z+7,1);
		world.setBlockWithNotify(x+5,y+2,z+4,1);
		world.setBlockWithNotify(x+5,y+2,z+5,1);
		world.setBlockWithNotify(x+5,y+2,z+6,1);
		world.setBlockWithNotify(x+5,y+2,z+7,1);
		world.setBlockWithNotify(x+5,y+3,z+4,1);
		world.setBlockWithNotify(x+5,y+3,z+5,1);
		world.setBlockWithNotify(x+5,y+3,z+6,1);
		world.setBlockWithNotify(x+5,y+3,z+7,1);
		world.setBlockWithNotify(x+5,y+3,z+8,1);
		world.setBlockWithNotify(x+5,y+4,z+4,1);
		world.setBlockWithNotify(x+5,y+4,z+5,1);
		world.setBlockWithNotify(x+5,y+4,z+6,1);
		world.setBlockWithNotify(x+5,y+4,z+7,1);
		world.setBlockWithNotify(x+5,y+4,z+8,1);
		world.setBlockWithNotify(x+5,y+5,z+2,1);
		world.setBlockWithNotify(x+5,y+5,z+3,5);
		world.setBlockWithNotify(x+5,y+5,z+4,5);
		world.setBlockWithNotify(x+5,y+5,z+5,5);
		world.setBlockWithNotify(x+5,y+5,z+6,5);
		world.setBlockWithNotify(x+5,y+5,z+7,5);
		world.setBlockWithNotify(x+5,y+5,z+8,1);
		world.setBlockWithNotify(x+5,y+6,z+2,45);
		world.setBlockWithNotify(x+5,y+6,z+3,45);
		world.setBlockWithNotify(x+5,y+6,z+4,45);
		world.setBlockWithNotify(x+5,y+6,z+5,45);
		world.setBlockWithNotify(x+5,y+6,z+6,45);
		world.setBlockWithNotify(x+5,y+6,z+7,45);
		world.setBlockWithNotify(x+5,y+7,z+2,45);
		world.setBlockWithNotify(x+5,y+7,z+3,45);
		world.setBlockWithNotify(x+5,y+7,z+4,45);
		world.setBlockWithNotify(x+5,y+7,z+5,45);
		world.setBlockWithNotify(x+5,y+7,z+6,45);
		world.setBlockWithNotify(x+5,y+7,z+7,45);
		world.setBlockWithNotify(x+5,y+8,z+2,45);
		world.setBlockWithNotify(x+5,y+8,z+3,45);
		world.setBlockWithNotify(x+5,y+8,z+4,45);
		world.setBlockWithNotify(x+5,y+8,z+5,45);
		world.setBlockWithNotify(x+5,y+8,z+6,45);
		world.setBlockWithNotify(x+5,y+8,z+7,45);
		world.setBlockWithNotify(x+5,y+9,z+2,108);
		world.setBlockWithNotify(x+5,y+9,z+3,108);
		world.setBlockWithNotify(x+5,y+9,z+4,108);
		world.setBlockWithNotify(x+5,y+9,z+5,108);
		world.setBlockWithNotify(x+5,y+9,z+6,108);
		world.setBlockWithNotify(x+5,y+9,z+7,108);
		world.setBlockWithNotify(x+6,y+0,z+4,1);
		world.setBlockWithNotify(x+6,y+0,z+5,1);
		world.setBlockWithNotify(x+6,y+0,z+6,1);
		world.setBlockWithNotify(x+6,y+0,z+7,1);
		world.setBlockWithNotify(x+6,y+1,z+4,1);
		world.setBlockWithNotify(x+6,y+1,z+5,1);
		world.setBlockWithNotify(x+6,y+1,z+6,1);
		world.setBlockWithNotify(x+6,y+1,z+7,1);
		world.setBlockWithNotify(x+6,y+2,z+4,1);
		world.setBlockWithNotify(x+6,y+2,z+5,46);
		world.setBlockWithNotify(x+6,y+2,z+6,46);
		world.setBlockWithNotify(x+6,y+2,z+7,1);
		world.setBlockWithNotify(x+6,y+3,z+4,1);
		world.setBlockWithNotify(x+6,y+3,z+5,46);
		world.setBlockWithNotify(x+6,y+3,z+6,46);
		world.setBlockWithNotify(x+6,y+3,z+7,1);
		world.setBlockWithNotify(x+6,y+3,z+8,1);
		world.setBlockWithNotify(x+6,y+4,z+4,1);
		world.setBlockWithNotify(x+6,y+4,z+5,46);
		world.setBlockWithNotify(x+6,y+4,z+6,46);
		world.setBlockAndMetadataWithNotify(x+6,y+4,z+7,93,12);
		world.setBlockWithNotify(x+6,y+4,z+8,55);
		world.setBlockWithNotify(x+6,y+5,z+2,1);
		world.setBlockWithNotify(x+6,y+5,z+3,5);
		world.setBlockWithNotify(x+6,y+5,z+4,5);
		world.setBlockWithNotify(x+6,y+5,z+5,5);
		world.setBlockWithNotify(x+6,y+5,z+6,5);
		world.setBlockWithNotify(x+6,y+5,z+7,5);
		world.setBlockWithNotify(x+6,y+5,z+8,1);
		world.setBlockWithNotify(x+6,y+6,z+2,45);
		world.setBlockAndMetadataWithNotify(x+6,y+6,z+3,54,3);
		world.setBlockWithNotify(x+6,y+6,z+8,70);
		world.setBlockWithNotify(x+6,y+7,z+2,45);
		world.setBlockWithNotify(x+6,y+8,z+2,45);
		world.setBlockWithNotify(x+6,y+8,z+7,45);
		world.setBlockWithNotify(x+6,y+9,z+2,45);
		world.setBlockWithNotify(x+6,y+9,z+7,45);
		world.setBlockWithNotify(x+6,y+10,z+2,108);
		world.setBlockWithNotify(x+6,y+10,z+3,108);
		world.setBlockWithNotify(x+6,y+10,z+4,108);
		world.setBlockWithNotify(x+6,y+10,z+5,108);
		world.setBlockWithNotify(x+6,y+10,z+6,108);
		world.setBlockWithNotify(x+6,y+10,z+7,108);
		world.setBlockWithNotify(x+7,y+0,z+4,1);
		world.setBlockWithNotify(x+7,y+0,z+5,1);
		world.setBlockWithNotify(x+7,y+0,z+6,1);
		world.setBlockWithNotify(x+7,y+0,z+7,1);
		world.setBlockWithNotify(x+7,y+1,z+4,1);
		world.setBlockWithNotify(x+7,y+1,z+5,1);
		world.setBlockWithNotify(x+7,y+1,z+6,1);
		world.setBlockWithNotify(x+7,y+1,z+7,1);
		world.setBlockWithNotify(x+7,y+2,z+4,1);
		world.setBlockWithNotify(x+7,y+2,z+5,46);
		world.setBlockWithNotify(x+7,y+2,z+6,46);
		world.setBlockWithNotify(x+7,y+2,z+7,1);
		world.setBlockWithNotify(x+7,y+3,z+4,1);
		world.setBlockWithNotify(x+7,y+3,z+5,46);
		world.setBlockWithNotify(x+7,y+3,z+6,46);
		world.setBlockWithNotify(x+7,y+3,z+7,1);
		world.setBlockWithNotify(x+7,y+3,z+8,1);
		world.setBlockWithNotify(x+7,y+4,z+4,1);
		world.setBlockWithNotify(x+7,y+4,z+5,46);
		world.setBlockWithNotify(x+7,y+4,z+6,46);
		world.setBlockWithNotify(x+7,y+4,z+7,1);
		world.setBlockWithNotify(x+7,y+4,z+8,1);
		world.setBlockWithNotify(x+7,y+5,z+2,1);
		world.setBlockWithNotify(x+7,y+5,z+3,5);
		world.setBlockWithNotify(x+7,y+5,z+4,5);
		world.setBlockWithNotify(x+7,y+5,z+5,5);
		world.setBlockWithNotify(x+7,y+5,z+6,5);
		world.setBlockWithNotify(x+7,y+5,z+7,5);
		world.setBlockWithNotify(x+7,y+5,z+8,1);
		world.setBlockWithNotify(x+7,y+6,z+2,45);
		world.setBlockAndMetadataWithNotify(x+7,y+6,z+3,54,3);
		this.spawnEntity(x+7, y+6, z+5, new EntityCreeper(world));
		world.setBlockWithNotify(x+7,y+6,z+7,45);
		world.setBlockWithNotify(x+7,y+7,z+2,45);
		world.setBlockWithNotify(x+7,y+7,z+7,45);
		world.setBlockWithNotify(x+7,y+8,z+2,45);
		world.setBlockWithNotify(x+7,y+8,z+7,45);
		world.setBlockWithNotify(x+7,y+9,z+2,45);
		world.setBlockWithNotify(x+7,y+9,z+7,45);
		world.setBlockAndMetadataWithNotify(x+7,y+10,z+2,108,1);
		world.setBlockAndMetadataWithNotify(x+7,y+10,z+3,108,1);
		world.setBlockAndMetadataWithNotify(x+7,y+10,z+4,108,1);
		world.setBlockAndMetadataWithNotify(x+7,y+10,z+5,108,1);
		world.setBlockAndMetadataWithNotify(x+7,y+10,z+6,108,1);
		world.setBlockAndMetadataWithNotify(x+7,y+10,z+7,108,1);
		world.setBlockWithNotify(x+8,y+0,z+4,1);
		world.setBlockWithNotify(x+8,y+0,z+5,1);
		world.setBlockWithNotify(x+8,y+0,z+6,1);
		world.setBlockWithNotify(x+8,y+0,z+7,1);
		world.setBlockWithNotify(x+8,y+1,z+4,1);
		world.setBlockWithNotify(x+8,y+1,z+5,1);
		world.setBlockWithNotify(x+8,y+1,z+6,1);
		world.setBlockWithNotify(x+8,y+1,z+7,1);
		world.setBlockWithNotify(x+8,y+2,z+4,1);
		world.setBlockWithNotify(x+8,y+2,z+5,1);
		world.setBlockWithNotify(x+8,y+2,z+6,1);
		world.setBlockWithNotify(x+8,y+2,z+7,1);
		world.setBlockWithNotify(x+8,y+3,z+4,1);
		world.setBlockWithNotify(x+8,y+3,z+5,1);
		world.setBlockWithNotify(x+8,y+3,z+6,1);
		world.setBlockWithNotify(x+8,y+3,z+7,1);
		world.setBlockWithNotify(x+8,y+3,z+8,1);
		world.setBlockWithNotify(x+8,y+4,z+4,1);
		world.setBlockWithNotify(x+8,y+4,z+5,1);
		world.setBlockWithNotify(x+8,y+4,z+6,1);
		world.setBlockWithNotify(x+8,y+4,z+7,1);
		world.setBlockWithNotify(x+8,y+4,z+8,1);
		world.setBlockWithNotify(x+8,y+5,z+2,1);
		world.setBlockWithNotify(x+8,y+5,z+3,5);
		world.setBlockWithNotify(x+8,y+5,z+4,5);
		world.setBlockWithNotify(x+8,y+5,z+5,5);
		world.setBlockWithNotify(x+8,y+5,z+6,5);
		world.setBlockWithNotify(x+8,y+5,z+7,5);
		world.setBlockWithNotify(x+8,y+5,z+8,1);
		world.setBlockWithNotify(x+8,y+6,z+2,45);
		world.setBlockWithNotify(x+8,y+6,z+3,45);
		world.setBlockWithNotify(x+8,y+6,z+4,45);
		world.setBlockWithNotify(x+8,y+6,z+5,45);
		world.setBlockWithNotify(x+8,y+6,z+6,45);
		world.setBlockWithNotify(x+8,y+6,z+7,45);
		world.setBlockWithNotify(x+8,y+7,z+2,45);
		world.setBlockWithNotify(x+8,y+7,z+3,45);
		world.setBlockWithNotify(x+8,y+7,z+4,45);
		world.setBlockWithNotify(x+8,y+7,z+5,45);
		world.setBlockWithNotify(x+8,y+7,z+6,45);
		world.setBlockWithNotify(x+8,y+7,z+7,45);
		world.setBlockWithNotify(x+8,y+8,z+2,45);
		world.setBlockWithNotify(x+8,y+8,z+3,45);
		world.setBlockWithNotify(x+8,y+8,z+4,45);
		world.setBlockWithNotify(x+8,y+8,z+5,45);
		world.setBlockWithNotify(x+8,y+8,z+6,45);
		world.setBlockWithNotify(x+8,y+8,z+7,45);
		world.setBlockAndMetadataWithNotify(x+8,y+9,z+2,108,1);
		world.setBlockAndMetadataWithNotify(x+8,y+9,z+3,108,1);
		world.setBlockAndMetadataWithNotify(x+8,y+9,z+4,108,1);
		world.setBlockAndMetadataWithNotify(x+8,y+9,z+5,108,1);
		world.setBlockAndMetadataWithNotify(x+8,y+9,z+6,108,1);
		world.setBlockAndMetadataWithNotify(x+8,y+9,z+7,108,1);
		world.setBlockAndMetadataWithNotify(x+6,y+6,z+7,71,3);
		world.setBlockAndMetadataWithNotify(x+6,y+7,z+7,71,11);

	}
}
