package okushama.personal.gen;

import java.util.Random;

//import okushama.humansplus.HumanStrongholdGuard;
import net.minecraft.src.Block;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Material;
import net.minecraft.src.World;

public class BuildingStrongholdOutpost extends BuildingBase{


	public boolean canGenerate(){
		int stored = y;
		for(int i = 0; i < 9; i++){
			if((world.getTopSolidOrLiquidBlock(x+i, z) != stored)){;
			return false;
			}else{
				if(world.getBlockMaterial(x+i, world.getTopSolidOrLiquidBlock(x+i,z), z)== Material.water){
					return false;
				}
				continue;
			}
		}
		for(int i = 0; i < 10; i++){
			if((world.getTopSolidOrLiquidBlock(x, z+i) != stored)){

				return false;
			}else{
				if(world.getBlockMaterial(x+i, world.getTopSolidOrLiquidBlock(x+i,z), z)== Material.water){
					return false;
				}
				continue;
			}

		}
		return true;

	}
	@Override
	public void generate(int x, int y, int z){
		world.setBlockWithNotify(x+2,y+0,z+2,98);
		world.setBlockWithNotify(x+2,y+0,z+10,98);
		world.setBlockAndMetadataWithNotify(x+2,y+3,z+8,106,8);
		world.setBlockWithNotify(x+2,y+4,z+2,98);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+3,44,5);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+4,44,5);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+5,44,5);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+6,44,5);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+7,44,5);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+8,44,5);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+9,44,5);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+10,98,2);
		world.setBlockAndMetadataWithNotify(x+3,y+0,z+3,98,2);
		world.setBlockWithNotify(x+3,y+0,z+4,98);
		world.setBlockWithNotify(x+3,y+0,z+5,98);
		world.setBlockAndMetadataWithNotify(x+3,y+0,z+6,98,2);
		world.setBlockAndMetadataWithNotify(x+3,y+0,z+7,44,5);
		world.setBlockWithNotify(x+3,y+0,z+8,98);
		world.setBlockAndMetadataWithNotify(x+3,y+0,z+9,98,2);
		world.setBlockWithNotify(x+3,y+1,z+3,98);
		world.setBlockWithNotify(x+3,y+1,z+4,98);
		world.setBlockWithNotify(x+3,y+1,z+5,98);
		world.setBlockWithNotify(x+3,y+1,z+6,98);
		world.setBlockWithNotify(x+3,y+1,z+8,98);
		world.setBlockWithNotify(x+3,y+1,z+9,98);
		world.setBlockWithNotify(x+3,y+2,z+3,98);
		world.setBlockWithNotify(x+3,y+2,z+4,101);
		world.setBlockWithNotify(x+3,y+2,z+5,101);
		world.setBlockWithNotify(x+3,y+2,z+6,98);
		world.setBlockWithNotify(x+3,y+2,z+8,98);
		world.setBlockWithNotify(x+3,y+2,z+9,98);
		world.setBlockWithNotify(x+3,y+3,z+3,98);
		world.setBlockWithNotify(x+3,y+3,z+4,101);
		world.setBlockWithNotify(x+3,y+3,z+5,101);
		world.setBlockAndMetadataWithNotify(x+3,y+3,z+6,98,2);
		world.setBlockWithNotify(x+3,y+3,z+7,101);
		world.setBlockWithNotify(x+3,y+3,z+8,98);
		world.setBlockAndMetadataWithNotify(x+3,y+3,z+9,98,2);
		world.setBlockAndMetadataWithNotify(x+3,y+4,z+2,44,5);
		world.setBlockAndMetadataWithNotify(x+3,y+4,z+3,98,2);
		world.setBlockAndMetadataWithNotify(x+3,y+4,z+4,98,2);
		world.setBlockAndMetadataWithNotify(x+3,y+4,z+5,98,2);
		world.setBlockAndMetadataWithNotify(x+3,y+4,z+6,98,2);
		world.setBlockWithNotify(x+3,y+4,z+7,98);
		world.setBlockWithNotify(x+3,y+4,z+8,98);
		world.setBlockAndMetadataWithNotify(x+3,y+4,z+9,98,1);
		world.setBlockAndMetadataWithNotify(x+3,y+4,z+10,44,5);
		world.setBlockAndMetadataWithNotify(x+4,y+0,z+3,98,2);
		world.setBlockAndMetadataWithNotify(x+4,y+0,z+4,98,2);
		world.setBlockAndMetadataWithNotify(x+4,y+0,z+5,98,1);
		world.setBlockAndMetadataWithNotify(x+4,y+0,z+6,98,1);
		world.setBlockAndMetadataWithNotify(x+4,y+0,z+7,98,1);
		world.setBlockWithNotify(x+4,y+0,z+8,98);
		world.setBlockWithNotify(x+4,y+0,z+9,98);
		world.setBlockWithNotify(x+4,y+1,z+3,98);
		world.setBlockAndMetadataWithNotify(x+4,y+1,z+4,26,10);
		world.setBlockAndMetadataWithNotify(x+4,y+1,z+5,26,2);
		world.setBlockWithNotify(x+4,y+1,z+6,98);
		world.setBlockAndMetadataWithNotify(x+4,y+1,z+8,98,2);
		world.setBlockAndMetadataWithNotify(x+4,y+1,z+9,98,1);
		world.setBlockWithNotify(x+4,y+2,z+3,101);
		world.setBlockWithNotify(x+4,y+2,z+4,26);
		world.setBlockAndMetadataWithNotify(x+4,y+2,z+5,26,8);
		world.setBlockWithNotify(x+4,y+2,z+6,98);
		world.setBlockWithNotify(x+4,y+2,z+8,98);
		world.setBlockWithNotify(x+4,y+2,z+9,101);
		world.setBlockWithNotify(x+4,y+3,z+3,101);
		world.setBlockWithNotify(x+4,y+3,z+6,98);
		world.setBlockWithNotify(x+4,y+3,z+7,98);
		world.setBlockWithNotify(x+4,y+3,z+8,98);
		world.setBlockWithNotify(x+4,y+3,z+9,101);
		world.setBlockAndMetadataWithNotify(x+4,y+4,z+2,44,5);
		world.setBlockAndMetadataWithNotify(x+4,y+4,z+3,98,1);

		// Double Chest
		world.setBlockAndMetadataWithNotify(x+4,y+4,z+4,54,5);
		world.setBlockAndMetadataWithNotify(x+4,y+4,z+5,54,5);
		

		world.setBlockWithNotify(x+4,y+4,z+6,58);
		world.setBlockAndMetadataWithNotify(x+4,y+4,z+7,61,5);
		world.setBlockAndMetadataWithNotify(x+4,y+4,z+8,61,5);
		world.setBlockAndMetadataWithNotify(x+4,y+4,z+9,98,1);
		world.setBlockAndMetadataWithNotify(x+4,y+4,z+10,44,5);
		world.setBlockAndMetadataWithNotify(x+4,y+5,z+4,44,5);
		world.setBlockAndMetadataWithNotify(x+4,y+5,z+5,44,5);
		world.setBlockAndMetadataWithNotify(x+4,y+5,z+6,44,5);
		world.setBlockAndMetadataWithNotify(x+4,y+5,z+7,44,5);
		world.setBlockAndMetadataWithNotify(x+4,y+5,z+8,44,5);
		world.setBlockWithNotify(x+5,y+0,z+3,98);
		world.setBlockAndMetadataWithNotify(x+5,y+0,z+4,98,1);
		world.setBlockAndMetadataWithNotify(x+5,y+0,z+5,98,2);
		world.setBlockWithNotify(x+5,y+0,z+6,98);
		world.setBlockWithNotify(x+5,y+0,z+7,98);
		world.setBlockAndMetadataWithNotify(x+5,y+0,z+8,98,2);
		world.setBlockWithNotify(x+5,y+0,z+9,98);
		world.setBlockWithNotify(x+5,y+1,z+3,98);
		world.setBlockAndMetadataWithNotify(x+5,y+1,z+9,98,2);
		world.setBlockWithNotify(x+5,y+2,z+3,101);
		world.setBlockWithNotify(x+5,y+2,z+9,101);
		world.setBlockWithNotify(x+5,y+3,z+3,101);
		world.setBlockWithNotify(x+5,y+3,z+9,101);
		world.setBlockAndMetadataWithNotify(x+5,y+4,z+2,44,5);
		world.setBlockAndMetadataWithNotify(x+5,y+4,z+3,98,1);
		world.setBlockWithNotify(x+5,y+4,z+9,98);
		world.setBlockAndMetadataWithNotify(x+5,y+4,z+10,44,5);
		world.setBlockAndMetadataWithNotify(x+5,y+5,z+4,44,5);
		world.setBlockAndMetadataWithNotify(x+5,y+5,z+5,44,5);
		world.setBlockAndMetadataWithNotify(x+5,y+5,z+6,44,5);
		world.setBlockAndMetadataWithNotify(x+5,y+5,z+7,44,5);
		world.setBlockAndMetadataWithNotify(x+5,y+5,z+8,44,5);
		world.setBlockWithNotify(x+6,y+0,z+3,98);
		world.setBlockWithNotify(x+6,y+0,z+4,98);
		world.setBlockWithNotify(x+6,y+0,z+5,98);
		world.setBlockAndMetadataWithNotify(x+6,y+0,z+6,98,2);
		world.setBlockAndMetadataWithNotify(x+6,y+0,z+7,98,1);
		world.setBlockWithNotify(x+6,y+0,z+8,98);
		world.setBlockWithNotify(x+6,y+0,z+9,98);
		world.setBlockWithNotify(x+6,y+1,z+3,98);
		world.setBlockAndMetadataWithNotify(x+6,y+1,z+9,98,1);
		world.setBlockWithNotify(x+6,y+2,z+3,101);
		world.setBlockWithNotify(x+6,y+2,z+9,101);
		world.setBlockWithNotify(x+6,y+3,z+3,101);
		world.setBlockWithNotify(x+6,y+3,z+9,101);
		world.setBlockAndMetadataWithNotify(x+6,y+4,z+2,44,5);
		world.setBlockWithNotify(x+6,y+4,z+3,98);
		world.setBlockWithNotify(x+6,y+4,z+9,98);
		world.setBlockAndMetadataWithNotify(x+6,y+4,z+10,44,5);
		world.setBlockAndMetadataWithNotify(x+6,y+5,z+4,44,5);
		world.setBlockAndMetadataWithNotify(x+6,y+5,z+5,44,5);
		world.setBlockAndMetadataWithNotify(x+6,y+5,z+6,44,5);
		world.setBlockAndMetadataWithNotify(x+6,y+5,z+7,44,5);
		world.setBlockAndMetadataWithNotify(x+6,y+5,z+8,44,5);
		world.setBlockWithNotify(x+7,y+0,z+3,98);
		world.setBlockAndMetadataWithNotify(x+7,y+0,z+4,98,2);
		world.setBlockAndMetadataWithNotify(x+7,y+0,z+5,98,2);
		world.setBlockAndMetadataWithNotify(x+7,y+0,z+6,98,2);
		world.setBlockAndMetadataWithNotify(x+7,y+0,z+7,98,2);
		world.setBlockAndMetadataWithNotify(x+7,y+0,z+8,98,2);
		world.setBlockAndMetadataWithNotify(x+7,y+0,z+9,98,2);
		world.setBlockWithNotify(x+7,y+1,z+3,98);
		world.setBlockAndMetadataWithNotify(x+7,y+1,z+4,98,1);
		world.setBlockAndMetadataWithNotify(x+7,y+1,z+5,98,1);
		world.setBlockAndMetadataWithNotify(x+7,y+1,z+6,98,2);
		world.setBlockAndMetadataWithNotify(x+7,y+1,z+7,98,1);
		world.setBlockWithNotify(x+7,y+1,z+8,98);
		world.setBlockAndMetadataWithNotify(x+7,y+1,z+9,98,2);
		world.setBlockWithNotify(x+7,y+2,z+3,98);
		world.setBlockWithNotify(x+7,y+2,z+4,101);
		world.setBlockWithNotify(x+7,y+2,z+5,101);
		world.setBlockWithNotify(x+7,y+2,z+6,101);
		world.setBlockWithNotify(x+7,y+2,z+7,101);
		world.setBlockWithNotify(x+7,y+2,z+8,101);
		world.setBlockWithNotify(x+7,y+2,z+9,98);
		world.setBlockAndMetadataWithNotify(x+7,y+3,z+2,106,1);
		world.setBlockAndMetadataWithNotify(x+7,y+3,z+3,98,1);
		world.setBlockWithNotify(x+7,y+3,z+4,101);
		world.setBlockWithNotify(x+7,y+3,z+5,101);
		world.setBlockWithNotify(x+7,y+3,z+6,101);
		world.setBlockWithNotify(x+7,y+3,z+7,101);
		world.setBlockWithNotify(x+7,y+3,z+8,101);
		world.setBlockAndMetadataWithNotify(x+7,y+3,z+9,98,1);
		world.setBlockAndMetadataWithNotify(x+7,y+4,z+2,44,5);
		world.setBlockAndMetadataWithNotify(x+7,y+4,z+3,98,2);
		world.setBlockAndMetadataWithNotify(x+7,y+4,z+4,98,1);
		world.setBlockAndMetadataWithNotify(x+7,y+4,z+5,98,2);
		world.setBlockAndMetadataWithNotify(x+7,y+4,z+6,98,2);
		world.setBlockWithNotify(x+7,y+4,z+7,98);
		world.setBlockWithNotify(x+7,y+4,z+8,98);
		world.setBlockAndMetadataWithNotify(x+7,y+4,z+9,98,2);
		world.setBlockAndMetadataWithNotify(x+7,y+4,z+10,44,5);
		world.setBlockWithNotify(x+8,y+0,z+2,98);
		world.setBlockWithNotify(x+8,y+0,z+10,98);
		world.setBlockAndMetadataWithNotify(x+8,y+1,z+4,106,2);
		world.setBlockAndMetadataWithNotify(x+8,y+3,z+3,106,2);
		world.setBlockWithNotify(x+8,y+4,z+2,98);
		world.setBlockAndMetadataWithNotify(x+8,y+4,z+3,44,5);
		world.setBlockAndMetadataWithNotify(x+8,y+4,z+4,44,5);
		world.setBlockAndMetadataWithNotify(x+8,y+4,z+5,44,5);
		world.setBlockAndMetadataWithNotify(x+8,y+4,z+6,44,5);
		world.setBlockAndMetadataWithNotify(x+8,y+4,z+7,44,5);
		world.setBlockAndMetadataWithNotify(x+8,y+4,z+8,44,5);
		world.setBlockAndMetadataWithNotify(x+8,y+4,z+9,44,5);
		world.setBlockWithNotify(x+8,y+4,z+10,98);
		world.setBlockAndMetadataWithNotify(x+1,y+4,z+2,50,2);
		world.setBlockAndMetadataWithNotify(x+1,y+4,z+10,50,2);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+1,50,4);
		world.setBlockAndMetadataWithNotify(x+2,y+4,z+11,50,3);
		world.setBlockAndMetadataWithNotify(x+3,y+1,z+7,77,3);
		world.setBlockAndMetadataWithNotify(x+4,y+1,z+7,71,0);
		world.setBlockAndMetadataWithNotify(x+4,y+2,z+7,71,8);
		world.setBlockAndMetadataWithNotify(x+5,y+2,z+8,77,1);
		world.setBlockAndMetadataWithNotify(x+8,y+4,z+1,50,4);
		world.setBlockAndMetadataWithNotify(x+8,y+4,z+11,50,3);
		world.setBlockAndMetadataWithNotify(x+9,y+4,z+2,50,1);
		world.setBlockAndMetadataWithNotify(x+9,y+4,z+10,50,1);
	//	this.spawnEntity(x+6,y+1,z+4, new HumanStrongholdGuard(world));
		System.out.println("World Generation Success!");
	}

}
