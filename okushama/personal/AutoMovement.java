package okushama.personal;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.EnumSet;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;


import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

public class AutoMovement{

	public Minecraft mc = Minecraft.getMinecraft();

	public static AutoMovement instance = null;
	public AutoMovement(){
		instance = this;
		try {
			bot = new Robot();
			bot.delay(3);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public int getTriggerKey(){
		if(mc.thePlayer.getCurrentEquippedItem() == null){
			return -10;
		}
		return (mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBow) ? 1: 0;
	}
	
	public double attackDist(){
		return getTriggerKey() == 0 ? mc.playerController.getBlockReachDistance() : 30D;
	}
	
	Robot bot;
	public int bowHoldDelay = 0;
	public boolean bowHeld = false;
	public boolean enabled = false;
	public void onTick(){
		try{
			if(!enabled && AutoPath.pathFollowing){
				AutoPath.stopPathing();
			}
			if(!path){
				AutoPath.stopPathing();
			}
			
			if(lockedEntity != null && lockedEntity.deathTime > 0){
				lockedEntity = null;
			}
			if(lockedEntity != null && Keyboard.isKeyDown(Keyboard.KEY_N)){
				lockedEntity = null;
			}
			if(mc.thePlayer != null){
				if(lockedEntity != null){
					double dist = mc.thePlayer.getDistanceToEntity(lockedEntity);
					if(Keyboard.isKeyDown(Keyboard.KEY_O)){
						if(lockedEntity.getHealth() > 0){
							AutoPath.pathTo(lockedEntity);
						}else{
							AutoPath.stopPathing();
						}
					}
					if(Keyboard.isKeyDown(Keyboard.KEY_S)){
						 if (mc.thePlayer.onGround)
			                {
			                    double var4 = lockedEntity.posX - mc.thePlayer.posX;
			                    double var6 = lockedEntity.posZ - mc.thePlayer.posZ;
			                    float var8 = MathHelper.sqrt_double(var4 * var4 + var6 * var6);
			                    mc.thePlayer.motionX = (var4 / (double)var8 * 0.5D * 0.8D + mc.thePlayer.motionX)*3;
			                    mc.thePlayer.motionZ = (var6 / (double)var8 * 0.5D * 0.8D + mc.thePlayer.motionZ)*3;
			                    mc.thePlayer.motionY = 0.5D;
			                }
					}
					if(AutoPath.pathFollowing && dist <= attackDist()){
						AutoPath.camLock = true;
						this.faceEntity(mc.thePlayer, lockedEntity, 15F+xModifier, 80F);
						int btnd = MouseEvent.BUTTON1_MASK;
						if(mc.inGameHasFocus){
						//	if(!bowHeld){
								bot.delay(30);
								bot.mousePress(btnd);
								if(btnd == MouseEvent.BUTTON1_MASK){
									bot.mouseRelease(btnd);
								}
								//if(btnd == MouseEvent.BUTTON3_MASK && !bowHeld && bowHoldDelay < 10){
								//	bowHeld = true;
								//}
						//	}
						}
					}
					if(dist > attackDist() && dist < 100D){
						AutoPath.camLock = false;
						if(path)
						AutoPath.pathTo(lockedEntity);
					}
				}
				if(Keyboard.isKeyDown(Keyboard.KEY_LBRACKET)){
					if(team == 0){
						 team = 1;
					}else{
						team = 0;
					}
				}
				if(Keyboard.isKeyDown(Keyboard.KEY_RBRACKET)){
					enabled = !enabled;
				}
				if(!enabled){
					return;
				}
				//if(mc.thePlayer.getCurrentEquippedItem() != null){
					//if(mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBow || mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemSword){
						double dist = /*(mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBow) ? 50D : 20D*/30D;
						EntityLiving ent = getClosestVisibleEntity(EntityZombie.class, dist, true);
						if(lockedEntity == null){
							lockedEntity = ent;
						}
						if(lockedEntity != null){
							trackLockedEntity();
							return;
						}
						if(ent != null){	
							if(mc.thePlayer.canEntityBeSeen(ent) && Mouse.isButtonDown(getTriggerKey())){
								this.faceEntity(mc.thePlayer, ent, 100F, 100F);
								return;
							}else{
							//	this.onScreenMSG = "";
							}
						}
					}
					
				//}
		//	}
			if(bowHeld){
				bowHoldDelay++;
				if(bowHoldDelay == 10){
					
					//bot.delay(3);
					//bot.mouseRelease(MouseEvent.BUTTON3_DOWN_MASK);
				//	bot = null;
					/*bot = new Robot();
					bot.delay(3);
					bot.mousePress(MouseEvent.BUTTON3_MASK);
					bot.mouseRelease(MouseEvent.BUTTON3_MASK);
					bot.mouseRelease(MouseEvent.BUTTON3_MASK);
					bot.mouseRelease(MouseEvent.BUTTON3_MASK);
					bot.mouseRelease(MouseEvent.BUTTON3_MASK);
					bot.mouseRelease(MouseEvent.BUTTON3_MASK);*/
					bowHoldDelay = 0;
					if(!Mouse.isButtonDown(1)){
					System.out.println("Fired a shot!");
					}
					bowHeld = false;
				}
			}
			this.onScreenMSG = "";
		}catch(Exception e){
			e.printStackTrace();
		}	
	}
	
	public float xModifier = 0F;
	
	public void trackLockedEntity(){
		if(Mouse.isButtonDown(getTriggerKey())){
			if(mc.objectMouseOver != null){
			//	PlayerPathUpdate.pathTo(mc.objectMouseOver.blockX, mc.objectMouseOver.blockY, mc.objectMouseOver.blockZ);
			}
			this.faceEntity(mc.thePlayer, lockedEntity, 15F+xModifier, 80F);
			xModifier += 0.3F;
		}else{
			xModifier = 0F;
		}
		if(lockedEntity.getHealth() < 1 || !mc.thePlayer.canEntityBeSeen(lockedEntity)){
			lockedEntity = null;
			xModifier = 0F;
		}
	}
	
	 public EntityLiving getClosestVisibleEntity(Class<? extends EntityLiving> c, double d, boolean all)
	    {
	        double offset = -1.0D;
	        EntityLiving theEnt = null;

	        for (int i = 0; i < mc.theWorld.loadedEntityList.size(); ++i)
	        {
	        	if(!(mc.theWorld.loadedEntityList.get(i) instanceof EntityLiving) || mc.theWorld.loadedEntityList.get(i) == mc.thePlayer){
	        		continue;
	        	}
	        	if(!(c == mc.theWorld.loadedEntityList.get(i).getClass()) && !all){
	        		continue;
	        	}		
	            EntityLiving var13 = (EntityLiving)mc.theWorld.loadedEntityList.get(i);
	            if(var13.isDead || var13.getHealth() < 1 || (var13 instanceof EntityPlayer)){
	            	continue;
	            }	 
	        /*	if(!(mc.theWorld.loadedEntityList.get(i) instanceof EntityPlayer)){
	        		if(team == 0)
	        		continue;
	        	}*/
		        	
	           /* if(var13 instanceof EntityPlayer){
	            	EntityPlayer player = (EntityPlayer) var13;
	            	if(team == 0 on Red Team){
	            		if(!(player.username.startsWith(blue))){
	            			continue;
	            		}
	            	}
	            	if(team == 1 on Blue Team){
	            		if(!(player.username.startsWith(red))){
	            			continue;
	            		}
	            	}
	            }*/
	            double var14 = var13.getDistanceSq(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ);
	            if(!mc.thePlayer.canEntityBeSeen(var13)){
	            	continue;
	            }
	            if(!enabled){
	            	continue;
	            }

	            if ((d < 0.0D || var14 < d * d) && (offset == -1.0D || var14 < offset))
	            {
	                offset = var14;
	                theEnt = var13;
	            }
	        }

	        return theEnt;
	    }
	
	public void faceEntity(Entity facer, Entity facee, float hSpeed,
			float vSpeed) {
		double var4 = facee.posX - facer.posX;
		double var8 = facee.posZ - facer.posZ;
		double var6;

		if (facee instanceof EntityLiving) {
			EntityLiving var10 = (EntityLiving) facee;
			var6 = facer.posY + (double) facer.getEyeHeight() - (var10.posY + (double) var10.getEyeHeight());
		} else {
			var6 = (facee.boundingBox.minY + facee.boundingBox.maxY) / 2.0D - (facer.posY + (double) facer.getEyeHeight());
		}

		double var14 = (double) MathHelper.sqrt_double(var4 * var4 + var8 * var8);
		float var12 = (float) (Math.atan2(var8, var4) * 180.0D / Math.PI) - 90.0F;
		float var13 = (float) (-(Math.atan2(var6, var14) * 180.0D / Math.PI));
		float yMod = 0F;
		double dist = facer.getDistanceToEntity(facee);
		//if(facee.posY > facer.posY){
			double diff = facee.posY - facer.posY;
			yMod -= (dist/8)+(diff > 0 ? diff : 0);
		//}
		facer.rotationPitch = updateRotation(facer.rotationPitch, -var13+yMod, vSpeed);
		facer.rotationYaw = updateRotation(facer.rotationYaw, var12, hSpeed);
		onScreenMSG = "Locked on to "+facee.getEntityName();
	}
	
	public static EntityLiving lockedEntity = null;
	
	public boolean isLocked(){
		return onScreenMSG.startsWith("Locked ");
	}
	
	
	public static float updateRotation(float par1, float par2, float par3) {
		float var4 = MathHelper.wrapAngleTo180_float(par2 - par1);

		if (var4 > par3) {
			var4 = par3;
		}

		if (var4 < -par3) {
			var4 = -par3;
		}

		return par1 + var4;
	}
	
	public String onScreenMSG = "";
	
	public String blue = "\u00A79";
	public String red = "\u00A7c";
	public int team = 0;
	public boolean path = false;
	/* 0 = red, 1 = blue */

	public String msg(){
		return (lockedEntity != null ? "Locked on to: "+lockedEntity.getEntityName() : "");
	}
	
	public String ttl(){
		return (mc.getIntegratedServer() != null ? "Enabled: "+enabled : "Current Team: "+(team > 0 ? blue+"Blue" : red+"Red")+"\u00A7f"+" | "+"Enabled: "+enabled);
	}

	public void onRenderTick(){
		if(mc.thePlayer != null){
		//	mc.ingameGUI.drawString(mc.fontRenderer, "Aim: "+enabled+" | Path: "+path, 5, 10, 0xFFFFFF);
		//	mc.ingameGUI.drawString(mc.fontRenderer, msg(), 5, 20, 0xFFFFFF);
		}
	}
}
