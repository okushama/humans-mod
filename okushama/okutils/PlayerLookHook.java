package okushama.okutils;

import net.minecraft.src.*;
import net.minecraft.client.*;

public class PlayerLookHook {
	
	public Minecraft mc;
	public PlayerLookHook(Minecraft m){
		mc = m;
	}
	
	public Entity getEntity(){
		try{
			if(mc.objectMouseOver.entityHit != null){
				return mc.objectMouseOver.entityHit;
			}
		}catch(Exception e){}
		return null;
	}
	
	public void setEntityPathTo(EntityCreature e, int x, int y, int z){
		e.setPathToEntity(e.worldObj.getEntityPathToXYZ(e, x, y, x, 16F, false, false, false, false));
	}
	
	public boolean isLookingAtEntity(){
		try{
			if(mc.objectMouseOver.entityHit != null){
				return true;
			}
		}catch(Exception e){}
		return false;
	}
	
	public int x() throws NullPointerException{
		return mc.objectMouseOver.blockX;
	}
	public int y() throws NullPointerException{
		return mc.objectMouseOver.blockY;
	}
	public  int z() throws NullPointerException{
		return mc.objectMouseOver.blockZ;
	}
	
	public int getBlockLookingAt(){
		try{
			return mc.theWorld.getBlockId(x(), y(), z());
			
		}catch(Exception e){}
		return 0;
	}
	
	public void setBlockLookingAt(int block){
		try{
			if(mc.thePlayer.getCurrentEquippedItem().itemID < 256 && mc.thePlayer.getCurrentEquippedItem().itemID > 0)
				mc.theWorld.setBlockWithNotify(x(), y(), z(), block);
		}catch(Exception e){e.printStackTrace();}
	}
	
	
	
	
	
	

}
