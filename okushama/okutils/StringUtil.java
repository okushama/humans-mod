package okushama.okutils;

import net.minecraft.src.FontRenderer;

public class StringUtil {
	
	private FontRenderer fontRenderer;
	public StringUtil(FontRenderer f){
		fontRenderer = f;
	}
	
	private String format(String in){
		String[] types = {"&m;�k", "&b;�l", "&s;�m", "&u;�n", "&i;�o", "&r;�r"};
		String[] colors = {"&black;�0", "&navy;�1", "&green;�2", "&aqua;�3", "&red;�4", "&purple;�5", "&orange;�6", "&gray;�7",
				           "&dgray;�8", "&cyan;�9", "&lime;�a", "&laqua;�b", "&rose;�c","&pink;�d","&yellow;�e", "&white;�f" };
		try{
			for(int i = 0; i < types.length; i++){
				String[] current = types[i].split(";");
				in = in.replaceAll(current[0], current[1]);
			}
			for(int i = 0; i < colors.length; i++){
				String[] current = colors[i].split(";");
				in = in.replaceAll(current[0], current[1]);
			}
		}catch(Exception e){ e.printStackTrace();}
		return in;
	}
	
	public void drawString(String s, int posX, int posY, boolean shadow){
		if(shadow)
			fontRenderer.drawStringWithShadow(format(s), posX, posY, 0xFFFFFF);
		else//(noShadow)
			fontRenderer.drawString(format(s), posX, posY, 0xFFFFFF);
		
	}

}
