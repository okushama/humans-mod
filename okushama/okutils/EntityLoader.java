package okushama.okutils;

import java.lang.reflect.Field;

import net.minecraft.client.Minecraft;
import net.minecraft.src.Entity;
import net.minecraft.src.ModLoader;
import net.minecraft.src.World;

public class EntityLoader {
	public static Minecraft mc = ModLoader.getMinecraftInstance();
	
	public static Entity newInstanceOfEnt(String classname){
		Class<? extends Entity> entityClass = null;
		Entity theEntity = null;
		try{
		entityClass = Class.forName(classname).asSubclass(Entity.class);
		theEntity = (Entity) entityClass.getConstructor(World.class).newInstance(mc.theWorld);
		}catch(Exception e){
			System.out.println("Could not init entity!  :  "+classname+".class does not exist!");
		}
		return theEntity;
	}
	
	public static Entity newInstanceOfEnt(Entity ent){
		Class<? extends Entity> entityClass = null;
		String entClassName = ent.getClass().getName();
		Entity theEntity = null;
		try{
		entityClass = Class.forName(entClassName).asSubclass(Entity.class);
		theEntity = (Entity) entityClass.getConstructor(World.class).newInstance(mc.theWorld);
		}catch(Exception e){
			System.out.println("Could not init entity!  :  "+entClassName+".class does not exist!");
		}
		return theEntity;
	}
	
	
	public static Entity cloneEntity(Entity ent){
		return newInstanceOfEnt(ent);
	}
	
	public static Entity cloneEntity(String entClass){
		return newInstanceOfEnt(entClass);
	}

}
