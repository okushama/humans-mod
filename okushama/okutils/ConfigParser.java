package okushama.okutils;

import java.util.ArrayList;

public class ConfigParser {
	
	private LocalReader theReader;
	public ConfigParser(LocalReader reader){
		theReader = reader;
		
	}
	public boolean getBooleanForSetting(String setting){
		return theReader.getValue(setting).equalsIgnoreCase("true");
	}
	
	public ArrayList<String> getStackedSettings(String prefix){
		ArrayList<String> out = new ArrayList<String>();
		for(int i = 0; i < theReader.lines.size(); i++){
			String setting = theReader.lines.get(i).split("=")[0];
			if(setting.contains(".")){
				String s = setting.split(".")[0];
				if(s.equalsIgnoreCase(prefix)){
					out.add(setting);
				}
			}
			
		}
		return out;
	}

}
