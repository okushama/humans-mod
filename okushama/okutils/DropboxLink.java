package okushama.okutils;

public class DropboxLink {
	
	
	public static String dropbox = "http://dl.dropbox.com/u/17362162/";
	public static String modsDir = "Mods/";
	
	public static String getDir(String s){
		if(s.equalsIgnoreCase("HumansPlus"))
			s = "Humans+";
		return dropbox+modsDir+s+"/";
	}
	public static String getData(String mod, String data){
		if(mod.equalsIgnoreCase("HumansPlus"))
			mod = "Humans+";
		return dropbox+modsDir+mod+"/"+"data"+"/"+data;
	}
}
