package okushama.okutils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import net.minecraft.client.Minecraft;
import net.minecraft.src.ModLoader;
import net.minecraft.src.Packet250CustomPayload;

public class Data {
	
	public String channel;
	public Data(String c){
		channel = c;
	}
	

	public void clientMessageOut(Object s){
		try{
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
	        DataOutputStream data = new DataOutputStream(bytes);
	        ObjectOutputStream oout = new ObjectOutputStream(data);
	        oout.writeObject(s);
			clientPacketOut(customPacket(bytes.toByteArray()));
		}catch(Exception e){
			
		}
	}
	
	public void serverMessageOut(Object s){
		try{
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
	        DataOutputStream data = new DataOutputStream(bytes);
	        ObjectOutputStream oout = new ObjectOutputStream(data);
	        oout.writeObject(s);
			serverPacketOut(customPacket(bytes.toByteArray()));
		}catch(Exception e){
			
		}
	}
	
	public void serverPacketOut(Packet250CustomPayload p){
		if(Minecraft.getMinecraft().getIntegratedServer() != null){
			Minecraft.getMinecraft().getIntegratedServer().getConfigurationManager().sendPacketToAllPlayers(p);
		}
	}
	
	public void clientPacketOut(Packet250CustomPayload p){
       	ModLoader.clientSendPacket(p);
	}
	
	public ObjectInputStream objectIn(Packet250CustomPayload p){
		 DataInputStream dataStream = new DataInputStream(new ByteArrayInputStream(p.data));
		 ObjectInputStream in = null;
	        try{
	        	in = new ObjectInputStream(dataStream);
	        }catch(Exception e){
	        	
	        }
		return in;
	}
	
	public DataInputStream dataIn(Packet250CustomPayload p){
		DataInputStream in = new DataInputStream(new ByteArrayInputStream(p.data));
		return in;
	}
	
	public Packet250CustomPayload customPacket(byte[] data){ 
		Packet250CustomPayload packet = new Packet250CustomPayload();
		packet.channel = channel;
		packet.data = data;
		packet.length = packet.data.length;
		return packet;
	}


}
