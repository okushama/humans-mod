package okushama.okutils;


import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import net.minecraft.client.Minecraft;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.ModLoader;

public abstract class GuiClassLoader{
	public static void replacements(){	
	}
	private static Minecraft mc = ModLoader.getMinecraftInstance();
	@SuppressWarnings("unchecked")
	public static void addExternalReplacement(String inClass, String classname){
		try{
			File file = new File(Minecraft.getMinecraftDir()+"/guireplacements/");
			if(mc.currentScreen.getClass() == Class.forName(inClass)){
				try {
					URL url = file.toURI().toURL(); 
					ClassLoader cl = Minecraft.class.getClassLoader();
					Method addUrl = URLClassLoader.class.getDeclaredMethod("addURL", new Class[]{URL.class});
					addUrl.setAccessible(true);
					if(!(cl instanceof URLClassLoader)){
						System.out.println("A fatal error has occured, skipping Gui Replacements!");
						return;
					}
					addUrl.invoke(cl, new Object[]{url});
					Class cls = cl.loadClass(classname);
					Object screen = cls.newInstance();
					if(screen instanceof GuiScreen)
						mc.displayGuiScreen((GuiScreen) screen);
					System.out.println("GuiReplacement Occured: "+inClass+" to "+classname);
				} catch (MalformedURLException e) {
					System.out.println("GuiReplacement found an invalid path!");
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					System.out.println("GuiReplacement could not find the class specified!");
					e.printStackTrace();
				}
			}
		}catch(Exception e){

		}
	}	
	public static boolean shouldCheck(){
		if(lastScreen == mc.currentScreen){
			return false;
		}
		lastScreen = mc.currentScreen;
		return mc.currentScreen != null;
	}
	private static GuiScreen lastScreen;
}
