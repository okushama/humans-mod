package okushama.okutils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;

import net.minecraft.client.Minecraft;

public class LocalReader implements Serializable{

	public LocalReader(String location, String file) {
		theDir = location;
		theFile = file;
	}
	
	public LocalReader(String location, String file, boolean read){
		theDir = location;
		theFile = file;
		readConfig();
	}

	public String theDir;
	public String theFile;
	public ArrayList<String> lines = new ArrayList<String>();

	public void addSetting(String setting, String value) {
		lines.add(setting + "=" + value);
	}
	
	public static String getModDir(String s){
		return Minecraft.getMinecraftDir()+"/okushama/"+s+"/";
	}

	public void readConfig() {
		lines = new ArrayList<String>();
		FileInputStream fStream = null;
		File f = new File(theDir, theFile);
		if(!f.exists() && f.mkdirs()){
			try {
				f.createNewFile();
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new DataOutputStream(new FileOutputStream(f))));
				out.write("#Generated or something!");
				out.flush();
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			fStream = new FileInputStream(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		DataInputStream dStream = new DataInputStream(fStream);
		BufferedReader in = new BufferedReader(new InputStreamReader(dStream));
		String line;
		try {
			while ((line = in.readLine()) != null) {
				if (!line.startsWith("#")) {
					lines.add(line);
				}
			}
			in.close();
		} catch (Exception e) {
		//	e.printStackTrace();
		}
	}

	public void writeConfig(String title) {
		try {
			FileOutputStream fStream;
			File f = new File(theDir, theFile);
			if (!f.exists() && !f.isDirectory()) {
				File newf = new File(new File(theDir), theFile);
				newf.createNewFile();
			}
			fStream = new FileOutputStream(f);
			DataOutputStream dStream = new DataOutputStream(fStream);
			BufferedWriter in = new BufferedWriter(new OutputStreamWriter(
					dStream));
			in.write("# " + title);

			in.newLine();

			for (int i = 0; i < lines.size(); i++) {
				in.write(lines.get(i));
				in.newLine();
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getValue(String setting) {
		String value = "";
		for (int i = 0; i < lines.size(); i++) {
			String[] param = lines.get(i).split("=");
			if (setting.equalsIgnoreCase(param[0])) {
				value = param[1];
				continue;
			}
		}
		if (value == "") {
		}
		return value;
	}
	
	public void replaceSetting(String setting, String value){
		for(int i = 0; i < lines.size(); i++){
			if(lines.get(i).startsWith(setting+"=")){
				lines.set(i, setting+"="+value);
			}
		}
	}

	public String getSetting(String value) {
		String setting = "";
		for (int i = 0; i < lines.size(); i++) {
			String[] param = lines.get(i).split("=");
			if (value.equalsIgnoreCase(param[1])) {
				setting = param[0];
				continue;
			}
		}
		if (setting == "") {
		}
		return setting;
	}
}
