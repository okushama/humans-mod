package okushama.okutils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WebReader {
	public List<String> lines;

	public WebReader(String url) {
		try {
			toLoad = new URL(url);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		readUrl(toLoad);
	}

	public void reload() {
		lines = new ArrayList<String>();
		readUrl(toLoad);
		for (int i = 0; i < lines.size(); i++)
			System.out.println("Line found: " + lines.get(i));
	}

	public void purgeLines() {
		if (lines != null) {
			int i;
			for (i = 0; i < lines.size(); i++)
				lines.remove(i);
		}
	}

	public void changeUrlTo(String s) {
		try {
			if (toLoad != null) {
				toLoad = null;
				lines = null;
			}
			try {
				purgeLines();
				toLoad = new URL(s);
				readUrl(toLoad);

			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {

		}

	}

	public boolean compareLine(String s) {
		for (int i = 0; i < lines.size(); i++) {
			if (s.equalsIgnoreCase(lines.get(i)))
				return true;
		}
		return false;
	}

	public String getLine(int i) {
		String s = "";
		if (lines != null && lines.size() >= i) {
			s = lines.get(i - 1);
		}
		return s;
	}

	public Random rand = new Random();

	public String getRandomLine() {
		String s = "";
		if (lines != null && lines.size() > 0) {
			s = lines.get(rand.nextInt(lines.size()));
		}
		return s;
	}

	public URL toLoad;

	public void readUrl(URL url) {
		lines = new ArrayList<String>();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String inputLine;
			int i = 0;
			while ((inputLine = in.readLine()) != null) {
				if (!inputLine.startsWith("#")) {
					lines.add(i, inputLine);
					i++;
				}
			}
			in.close();
		} catch (Exception e) {
		}
	}
}
