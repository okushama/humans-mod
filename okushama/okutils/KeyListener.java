package okushama.okutils;

public interface KeyListener{
	

	public void keyPressed(int key);
	
	public void keyHeld(int key);
	
	public void keyReleased(int key);

}
