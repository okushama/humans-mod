package okushama;


import java.util.ArrayList;
import java.util.Random;

import net.minecraft.src.*;
import okushama.humansplus.HumanRogue;
import okushama.personal.gen.*;

import org.lwjgl.input.Keyboard;

public class Schematic{

	public static Block schematicBlock;


	public int[] rarities = {5,5};

	public void generateSurface(World world, Random rand, int chunkX, int chunkZ) {
		if(world == null){return;}
		try{
			BuildingBase[] buildings = {new BuildingRestStop(), new BuildingStrongholdOutpost(), /*new BuildingBoom()*/};

			for(int pie = 0; pie < buildings.length; pie++){
				if(rand.nextInt(rarities[pie]) == 0){
						try{
							BuildingBase b = buildings[pie];
							b.x = chunkX+(rand.nextBoolean() ? 64+rand.nextInt(64)*rand.nextInt(16) : -64-(rand.nextInt(64)*rand.nextInt(16)));
							b.z = chunkZ+(rand.nextBoolean() ? 64+rand.nextInt(64)*rand.nextInt(16) : -64-(rand.nextInt(64)*rand.nextInt(16)));
							b.y = world.getTopSolidOrLiquidBlock(b.x, b.z);

							if(b.canGenerate()){
								b.doGenerate(b.x,b.y,b.z);
								return;
							}
							continue;
						}catch(Exception e){
							e.printStackTrace();
						}
				}else{
					continue;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("!!!!!!!!!! NO GEN !!!!!!!!!!!!");
	}

	public double randpos(double i){
		Random rand = new Random();
		return (int)i+(int)MathHelper.getRandomIntegerInRange(rand,-10,10);
	}


	public void init() {
		schematicBlock = new SchematicBlock(199,5).setBlockName("Reader Block");
		ModLoader.addName(schematicBlock, "Reader Block");
		ModLoader.registerBlock(schematicBlock);
		System.out.println("Registered Block!");
	}


}
