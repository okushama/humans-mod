var spawnType = vendor.getEntityName();
//var thePlayer = mc.thePlayer;

function quest_init(){
	// Teleports the npc to the player (debug purposes)
	out.println("JS_INIT");
	//vendor.setPosition(thePlayer.posX, thePlayer.posY, thePlayer.posZ);
	//vendor.moveSpeed = 1.4;
}

function quest_start(){
	// Let's make sure the vendor can keep up with us!
	//vendor.moveSpeed = 1.3;
}

function scout_update(){
	// If he's further than 10 blocks away, path towards us!
	if(helper.distance(vendor, vendee) > 4){
		vendor.a(helper.pathFromTo(vendor, vendee));
	}
	//vendor.moveSpeed = 1.4;
}

var hunters = new Array();

// once quest requirements are met, this updates making the
// new hunters walk to the vendor
function scout_reqmet_update(){
	for(var i = 0; i < hunters.length; i++){
		var hunter = hunters[i];
		if(helper.distance(hunter, vendor) > 5){
		hunter.a(helper.pathFromTo(hunter, vendor));
		}
	}
}

function quest_complete(){
	
}

// when requirements are met, spawn in some hunters!
function requirements_met(){
	for(var i = 0; i < 7; i++){
		hunters[hunters.length] = helper.createHumanInWorld(spawnType, vendor.posX, vendor.posY, vendor.posZ, true);
	}
}

function quest_quit(){

}